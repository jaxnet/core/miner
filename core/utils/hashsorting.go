package utils

import (
	"math/big"
)

// ChainIDCount = 2^12
const ChainIDCount = 4096

func HashSortingLastBits(diff *big.Int, chainIDCount uint32) uint32 {
	lastBitsMask := new(big.Int).SetUint64(uint64(chainIDCount) - 1)

	res := diff.And(diff, lastBitsMask)
	bits := res.Bits()
	if len(bits) > 0 {
		if uint32(bits[0]) < chainIDCount {
			return uint32(bits[0])
		} else {
			return uint32(bits[0]) % chainIDCount
		}
	}

	return 0
}
