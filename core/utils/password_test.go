package utils

import (
	"fmt"
	"testing"
)

func TestExtractOptionsFromPassword(t *testing.T) {
	t.Log("Testing parse of password parameters.")

	type test struct {
		password string
		options  map[string]float64
	}

	tests := []test{
		{
			password: "d",
			options:  nil,
		},
		{
			password: "d=10",
			options: map[string]float64{
				"d": 10,
			},
		},
		{
			password: "d=10,p=15",
			options: map[string]float64{
				"d": 10,
				"p": 15,
			},
		},
		{
			password: "d=10,p=15,s=30",
			options: map[string]float64{
				"d": 10,
				"p": 15,
				"s": 30,
			},
		},
		{
			password: "d=10,p=15,   s=30,test",
			options: map[string]float64{
				"d": 10,
				"p": 15,
				"s": 30,
			},
		},
	}

	for _, test := range tests {
		options, _ := ExtractOptionsFromPassword(test.password)
		if fmt.Sprint(options) != fmt.Sprint(test.options) {
			t.Fatalf("Fail on invalid expression, want: %+v, got: %+v\n", test.options, options)
		}
	}
}
