package integration

import (
	"bytes"
	"encoding/hex"
	"math/big"
	"testing"
	"time"

	btcdwire "github.com/btcsuite/btcd/wire"
	"github.com/davecgh/go-spew/spew"
	"github.com/patrickmn/go-cache"
	"gitlab.com/jaxnet/core/miner/core/logger"
	"gitlab.com/jaxnet/core/miner/core/miner/tasks"
	"gitlab.com/jaxnet/core/miner/core/settings"
	"gitlab.com/jaxnet/core/miner/core/stratum"
	"gitlab.com/jaxnet/core/miner/core/utils"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
	"gitlab.com/jaxnet/jaxnetd/types/wire"
)

// 10 minutes and 1 minute
const avgBitcoinBlockMiningIntervalExpiration = 10 + 1

// Purge 5 minutes after expiration
const avgBitcoinBlockMiningIntervalPurge = avgBitcoinBlockMiningIntervalExpiration + 5

func TestStratumSolver(t *testing.T) {
	// 1. fetch notify
	// 2. try to solve
	lp := logger.LogParams{
		JSONMode:   false,
		Debug:      false,
		OutputFile: "",
		Level:      "info",
	}
	logger.Init(lp)

	jobMan := &stratum.JobManager{
		Config:                new(settings.Configuration),
		JobCounter:            nil,
		ExtraNoncePlaceholder: nil,
		ExtraNonceSize:        0,
		CurrentJob:            stratum.Job{},
		ValidJobsCache:        cache.New(avgBitcoinBlockMiningIntervalExpiration*time.Minute, avgBitcoinBlockMiningIntervalPurge*time.Minute),
		CoinbaseHasher:        nil,
		MinerResultChan:       nil,
	}
	block := getBlock()
	block, _ = utils.UpdateBitcoinExtraNonce(block, 0x42, 0xBAD, chainhash.ZeroHash.CloneBytes())

	job := stratum.Job{
		MinerTask: tasks.MinerTask{
			BeaconBlock:        wire.MsgBlock{},
			BeaconBlockHeight:  0,
			BeaconTarget:       *big.NewInt(0),
			ShardsTargets:      nil,
			BitcoinBlock:       block,
			BitcoinBlockHeight: 0x42,
			// BitcoinBlockBits:   block.Header.Bits,
			// BitcoinBlockTarget: pow.CompactToBig(block.Header.Bits),
		},
		JobId:                 "bfg",
		PrevHashReversed:      "",
		Submits:               nil,
		GenerationTransaction: nil,
		Broadcasted:           false,
	}
	_ = job

	params, err := jobMan.GetJobParamsEx(job, false)
	if err != nil {
		t.Log("error:", err.Error())
		t.FailNow()
	}
	_ = params

}

func getBlock() btcdwire.MsgBlock {
	blockData := "00000020f67ad7695d9b662a72ff3d8edbbb2de0bfa67b13974bb9910d116d5cbd863e682465956c3386cd9dec8520e61aeabb52913eb996cffc6c5b09517aedcdecec27a4a0ed60ffff7f20000000000101000000010000000000000000000000000000000000000000000000000000000000000000ffffffff125100000e2f503253482f6a61786e6574642fffffffff030000000000000000176a152068747470733a2f2f6a61782e6e6574776f726b2000f2052a01000000176a1520202020202020204a41582020202020202020202000000000000000001976a914b953dad0e79288eea918085c9b72c3ca5482349388ac00000000"
	raw, _ := hex.DecodeString(blockData)
	buf := bytes.NewBuffer(raw)
	block := btcdwire.MsgBlock{}
	_ = block.Deserialize(buf)
	return block
}

func TestSand(t *testing.T) {
	rawHeader := "00000020f67ad7695d9b662a72ff3d8edbbb2de0bfa67b13974bb9910d116d5cbd863e680c9281f181c20a401fbacd91088a52f2454acd762cbd07da8031268e75c93a5fa4a0ed60ffff0f1e4be94c8e"
	rawH, _ := hex.DecodeString(rawHeader)
	bufH := bytes.NewBuffer(rawH)
	header := btcdwire.BlockHeader{}
	err := header.Deserialize(bufH)
	if err != nil {
		println(err.Error())
	}
	spew.Dump(header)

	rawTx, _ := hex.DecodeString("01000000010000000000000000000000000000000000000000000000000000000000000000ffffffff395108afbeadde01000000200ae00b032be5a3611203a9ba7780683917ce0bc184cad3a867c1121170bda8150d2f503253482f6a61786e65742fffffffff030000000000000000176a152068747470733a2f2f6a61782e6e6574776f726b2000f2052a01000000176a1520202020202020204a41582020202020202020202000000000000000001976a914b953dad0e79288eea918085c9b72c3ca5482349388ac00000000")
	bufTx := bytes.NewBuffer(rawTx)
	tx := btcdwire.MsgTx{}
	err = tx.Deserialize(bufTx)
	if err != nil {
		println(err.Error())
	}

	println(tx.TxHash().String())
	spew.Dump(tx)
}
