package stratum

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/pprof"
	_ "net/http/pprof"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/jaxnet/core/miner/core/logger"
	"gitlab.com/jaxnet/core/miner/core/settings"
	"gitlab.com/jaxnet/core/miner/core/storage"
)

// These constants define the application version and follow the semantic
// versioning 2.0.0 spec (http://semver.org/).
const (
	appMajor uint = 0
	appMinor uint = 0
	appPatch uint = 1
)

// version returns the application version as a properly formed string per the
// semantic versioning 2.0.0 spec (http://semver.org/).
func version() string {
	// Start with the major, minor, and patch versions.
	version := fmt.Sprintf("%d.%d.%d", appMajor, appMinor, appPatch)
	return version
}

type API struct {
	name    string
	version string
	options settings.APIServerConfig
	routes  *mux.Router
}

// newAPIServer returns initialised instance of API server
func newAPIServer(options settings.APIServerConfig) *API {
	api := new(API)
	api.name = "JAX mining pool API"
	api.version = version()

	// Options
	api.options = options

	// Routes
	api.routes = api.initRoutes()

	return api
}

func (api *API) initRoutes() *mux.Router {
	r := mux.NewRouter()
	api.setPublicRoutes(r)

	return r
}

func (api *API) Run() {
	logger.Log.Info().Str("host", api.options.Host).Str("port", api.options.Port).
		Str("version", api.version).Msg("Stratum server API starting")

	http.Handle("/", api.routes)
	logger.Log.Fatal().Err(http.ListenAndServe(api.options.Host+":"+api.options.Port, api.routes)).Msg("Failed to start stratum server API")
}

func (api *API) setPublicRoutes(r *mux.Router) {
	notAuthorized := r.PathPrefix("/api/v1").Subrouter()
	notAuthorized.HandleFunc("/stats", api.stats).Methods("GET")

	if api.options.EnableHealthEndpoint {
		notAuthorized.HandleFunc("/healthz", api.healthz).Methods("GET")
	}

	if api.options.EnableMetrics {
		r.Handle("/metrics", promhttp.Handler()).Methods("GET")
	}

	if api.options.EnablePProf {
		r.PathPrefix("/debug/pprof/").HandlerFunc(pprof.Index)
	}
}

func (api *API) healthz(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write([]byte("ok\n"))
}

func (api *API) stats(w http.ResponseWriter, r *http.Request) {
	// TODO: decide how to handle statistics.
	// Ether use metrics only or duplicate metrics in API.
	type respData struct {
		SharesTotal      map[string]Shares
		SharesLastMinute map[string]Shares
		WorkersActivity  map[string]Activity
		OrphanBlocks     map[string]string
	}

	result, err := storage.HGetAll(storage.OrphanBlocks)
	if err != nil {
		logger.Log.Error().Err(err).Msg("Failed to read data from Redis")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	data := respData{
		SharesTotal:      make(map[string]Shares),
		SharesLastMinute: make(map[string]Shares),
		WorkersActivity:  make(map[string]Activity),
		OrphanBlocks:     result,
	}

	bytes, err := json.Marshal(data)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(bytes)
	_, _ = w.Write([]byte("\n"))
}
