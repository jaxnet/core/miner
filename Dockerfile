# Copyright (c) 2020 The JaxNetwork developers
# Use of this source code is governed by an ISC
# license that can be found in the LICENSE file.

# Compile stage
FROM golang:alpine AS build-env

ENV GOPROXY=direct
ENV GO111MODULE=on
ENV GOPRIVATE=gitlab.com


WORKDIR /jax-stratum
ADD . .
RUN apk add --no-cache git bash && \
    go build -o /stratum

# Final stage
FROM alpine:3.15

# Allow delve to run on Alpine based containers.
RUN apk add --no-cache ca-certificates bash

WORKDIR /jax-stratum

COPY --from=build-env /stratum /

# Run app
CMD ./stratum
