package utils

import (
	"errors"
	"regexp"
	"strconv"
)

func ExtractOptionsFromPassword(password string) (map[string]float64, error) {
	r := regexp.MustCompile(`,?([[:alpha:]]+)=([0-9.]+)`)
	res := r.FindAllStringSubmatch(password, -1)
	if res != nil {
		options := make(map[string]float64, len(res))
		for i := range res {
			key := res[i][1]
			data := res[i][2]
			value, err := strconv.ParseFloat(data, 64)
			if err != nil {
				return nil, err
			}
			options[key] = value
		}
		return options, nil
	} else {
		return nil, errors.New("invalid format")
	}
}
