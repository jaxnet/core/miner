/*
 * Copyright (c) 2020 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package communicator

import (
	"fmt"
	"math/rand"
	"time"

	btcdjson "github.com/btcsuite/btcd/btcjson"
	btcdrpcclient "github.com/btcsuite/btcd/rpcclient"
	"github.com/rs/zerolog"
	"gitlab.com/jaxnet/core/miner/core/common"
	"gitlab.com/jaxnet/core/miner/core/communicator/events"
	"gitlab.com/jaxnet/core/miner/core/logger"
	"gitlab.com/jaxnet/jaxnetd/network/rpcclient"
	"gitlab.com/jaxnet/jaxnetd/types/jaxjson"
)

var (
	requestOptions = &jaxjson.TemplateRequest{
		Mode: "template",
		Capabilities: []string{
			"coinbasetxn",
		},
		// capability:
		//  case "coinbasetxn":      hasCoinbaseTxn = true
		//  case "coinbasevalue":    hasCoinbaseValue = true
		//  case "burnbtcreward":    burnReward |= types.BurnBtcReward
		//  case "burnjaxnetreward": burnReward |= types.BurnJaxNetReward
		//  case "burnjaxreward":    burnReward |= types.BurnJaxReward

	}

	bitcoinRequestOptions = &btcdjson.TemplateRequest{
		Mode: "template",
		Capabilities: []string{
			"coinbasevalue",
			"coinbasetxn",
		},
		WorkID: "coinbase/append",
		Rules:  []string{"segwit"},
	}
)

func (c *Communicator) handleBlockCandidatesPolling() {
	const beaconChainID = 0

	if c.config.EnableBTCMining {
		go c.handleBitcoinBlockCandidatePolling()
	}

	go c.handleBlockCandidatePolling(beaconChainID)

	// todo: https://gitlab.com/jaxnet/core/miner/-/issues/12
	for _, shard := range c.config.Shards {
		randomLaunchDelay := time.Duration(rand.Intn(100)) * time.Millisecond
		time.Sleep(randomLaunchDelay)

		go c.handleBlockCandidatePolling(shard.ID)
	}

}

func (c *Communicator) handleBlockCandidatePolling(shardID common.ShardID) {
	// Shortcut for checking if the method is expected to process beacon chain.
	// By default, there is no shard with ID == 0,
	// so it can be used to identify the beacon chain.
	isBeacon := shardID == 0

	fetchCandidateVia := func(client *rpcclient.Client) (candidate interface{}, err error) {
		if isBeacon {
			c.pollerLogTrace().Msg("Fetching next beacon block candidate")
			candidate, err = c.fetchBeaconBlockCandidate(client)
			if err != nil {
				c.pollerLogErr(err).Msg("Can't fetch beacon block candidate")
			}
		} else {
			c.pollerLogTrace().Uint32("ShardID", uint32(shardID)).Msg("Fetching next shard block candidate")
			candidate, err = c.fetchShardBlockCandidate(client, shardID)
			if err != nil {
				c.pollerLogErr(err).Uint32("shard-id", uint32(shardID)).Msg("Can't fetch shard block candidate")
			}
		}

		return
	}

	sendBlockTemplateForProcessing := func(block interface{}) {
		if isBeacon {
			candidate := block.(*jaxjson.GetBlockTemplateResult)
			c.beaconBlockCandidates <- events.BeaconBlockCandidate{Candidate: candidate}

			if c.config.Log.Debug {
				c.pollerLogTrace().
					Int64("height", candidate.Height).
					Msg("Beacon block candidate received")
			}

		} else {
			candidate := block.(*jaxjson.GetBlockTemplateResult)
			c.shardsBlockCandidates <- events.ShardBlockCandidate{Candidate: candidate, ShardID: shardID}

			if c.config.Log.Debug {
				c.pollerLogTrace().
					Int64("height", candidate.Height).
					Uint32("shard-id", uint32(shardID)).
					Msg("Shard block candidate received")
			}
		}
	}

	// todo: https://gitlab.com/jaxnet/core/miner/-/issues/3
	sleepUntilNextRound := func() {
		if isBeacon {
			time.Sleep(time.Second * pollingIntervalSeconds)
		} else {
			time.Sleep(time.Second * pollingIntervalSeconds)
		}
	}

	fetcher := fmt.Sprint("shard #", shardID)
	if isBeacon {
		fetcher = "beacon"
	}

	defer func() {
		// Log when polling goroutine would be finished.
		if c.config.Log.Debug {
			message := fmt.Sprint("Block candidates polling from ", fetcher, " stopped")
			c.senderLogTrace().Msg(message)
		}
	}()

	message := fmt.Sprint("Block candidates polling from ", fetcher, " started")
	c.senderLogTrace().Msg(message)

	// Notice:
	// for loop is used to restore the RPC-connection in case of disconnection/error.
	// (infinite polling mechanics)
	for {
		if c.MustBeStopped {
			return
		}

		client := c.getJaxNodeClient()
		if client == nil {
			c.pollerLogTrace().Msg("[handleBlockCandidatePolling] can't get Jax RPC client")
			time.Sleep(time.Second * pollingIntervalSeconds)
			continue
		}

		for {
			if c.MustBeStopped {
				return
			}

			candidate, err := fetchCandidateVia(client)
			if err != nil {
				c.pollerLogErr(err).Msg("Can't fetch block candidate")
				time.Sleep(time.Second * pollingIntervalSeconds)
				break
			}

			sendBlockTemplateForProcessing(candidate)
			sleepUntilNextRound()
		}

		// Internal for loop has ended with error.
		// It should be already logged now, so no any additional action is expected.
	}
}

func (c *Communicator) handleBitcoinBlockCandidatePolling() {
	fetchCandidateVia := func(client *btcdrpcclient.Client) (candidate *btcdjson.GetBlockTemplateResult, err error) {
		candidate, err = c.fetchBitcoinBlockCandidate(client)
		if err != nil {
			c.pollerLogErr(err).Msg("Can't fetch bitcoin block candidate")
		}
		return
	}

	sendBlockTemplateForProcessing := func(candidate *btcdjson.GetBlockTemplateResult) {
		c.bitcoinBlockCandidates <- events.BitcoinBlockCandidate{
			Candidate: candidate,
		}

		c.pollerLogTrace().
			Int("txs", len(candidate.Transactions)).
			Int64("height", candidate.Height).
			Str("bits", candidate.Bits).
			Msg("Bitcoin block candidate received")
	}

	// todo: https://gitlab.com/jaxnet/core/miner/-/issues/3
	sleepUntilNextRound := func() {
		time.Sleep(time.Second * pollingIntervalSeconds)
	}

	fetcher := "bitcoin"

	defer func() {
		// Log when polling goroutine would be finished.
		if c.config.Log.Debug {
			message := fmt.Sprint("Block candidates polling from ", fetcher, " stopped")
			c.senderLogTrace().Msg(message)
		}
	}()

	message := fmt.Sprint("Block candidates polling from ", fetcher, " started")
	c.senderLogTrace().Msg(message)

	// Notice:
	// for loop is used to restore the RPC-connection in case of disconnection/error.
	// (infinite polling mechanics)
	for {
		if c.MustBeStopped {
			return
		}

		client := c.getBtcNodeClient()
		if client == nil {
			c.pollerLogTrace().Msg("[handleBitcoinBlockCandidatePolling] can't get Bitcoin RPC client")
			time.Sleep(time.Second * pollingIntervalSeconds)
			continue
		}

		go func() {
			if c.MustBeStopped {
				return
			}

			for {
				res := <-c.StratumUpdateBlock
				c.pollerLogTrace().Int64("ChainID", res.ChainID).Msg("Received value in StratumUpdateBlock channel")

				if res.ChainID == -1 {
					c.pollerLogTrace().Msg("Fetching bitcoin block candidate after receiving value in StratumUpdateBlock channel")
					candidate, err := fetchCandidateVia(client)
					if err != nil {
						c.pollerLogErr(err).Msg("Can't fetch bitcoin block candidate")
						time.Sleep(time.Second * pollingIntervalSeconds)
						break
					}

					sendBlockTemplateForProcessing(candidate)
				}
			}
		}()

		for {
			if c.MustBeStopped {
				return
			}

			c.pollerLogTrace().Msg("Fetching next bitcoin block candidate")
			candidate, err := fetchCandidateVia(client)
			if err != nil {
				c.pollerLogErr(err).Msg("Can't fetch bitcoin block candidate")
				time.Sleep(time.Second * pollingIntervalSeconds)
				break
			}

			sendBlockTemplateForProcessing(candidate)
			sleepUntilNextRound()
		}

		// Internal for loop has ended with error.
		// It should be already logged now, so no any additional action is expected.
	}
}

// todo: https://gitlab.com/jaxnet/core/miner/-/issues/4
func (c *Communicator) fetchBeaconBlockCandidate(client *rpcclient.Client) (candidate *jaxjson.GetBlockTemplateResult, err error) {
	candidate, err = client.ForBeacon().GetBlockTemplate(requestOptions)
	return
}

// todo: https://gitlab.com/jaxnet/core/miner/-/issues/4
func (c *Communicator) fetchShardBlockCandidate(client *rpcclient.Client, shardID common.ShardID) (candidate *jaxjson.GetBlockTemplateResult, err error) {
	// WARN: [type overflow]
	//       Potential type overflow here in case if "shardID" would change its base type.
	candidate, err = client.ForShard(uint32(shardID)).GetBlockTemplate(requestOptions)
	return
}

func (c *Communicator) fetchBitcoinBlockCandidate(client *btcdrpcclient.Client) (candidate *btcdjson.GetBlockTemplateResult, err error) {
	candidate, err = client.GetBlockTemplate(bitcoinRequestOptions)
	return
}

func (c *Communicator) pollerLogInfo() *zerolog.Event {
	return logger.Log.Info().Str("component", "communicator/poller")
}

func (c *Communicator) pollerLogTrace() *zerolog.Event {
	return logger.Log.Trace().Str("component", "communicator/poller")
}

func (c *Communicator) pollerLogErr(err error) *zerolog.Event {
	return logger.Log.Err(err).Str("component", "communicator/poller")
}
