/*
 * Copyright (c) 2020 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package state

import (
	"bytes"
	"encoding/hex"
	"errors"
	"fmt"
	"math/big"
	"strconv"
	"sync"
	"time"

	"github.com/btcsuite/btcd/blockchain"
	btcdjson "github.com/btcsuite/btcd/btcjson"
	btcdchainhash "github.com/btcsuite/btcd/chaincfg/chainhash"
	btcdmining "github.com/btcsuite/btcd/mining"
	btcdwire "github.com/btcsuite/btcd/wire"
	btcdutil "github.com/btcsuite/btcutil"
	"gitlab.com/jaxnet/core/miner/core/common"
	"gitlab.com/jaxnet/core/miner/core/e"
	"gitlab.com/jaxnet/core/miner/core/logger"
	"gitlab.com/jaxnet/core/miner/core/utils"
	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/jaxnet/jaxnetd/node/chaindata"
	"gitlab.com/jaxnet/jaxnetd/types"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
	"gitlab.com/jaxnet/jaxnetd/types/jaxjson"
	"gitlab.com/jaxnet/jaxnetd/types/wire"
)

var (
	// Fixture date is used as a placeholder for block generation timestamp set in block's header.
	// Because of the deduplication mechanics that is applied to the blocks,
	// it is important to keep deserialized blocks free from volatile data
	// (like constantly changing timestamp).
	// Otherwise, deduplication fails even if original blocks are totally the same.
	fixtureDateTime, _ = time.Parse(time.RFC3339, "2012-11-01T22:08:41+00:00")
	fixtureMMProofHash = chainhash.Hash{}
	fixtureNonce       = uint32(0)

	lastBCHeader      *wire.BeaconHeader
	lastBCCoinbaseAux wire.CoinbaseAux
	lastBCHeaderMutex sync.Mutex
)

func (h *Coordinator) decodeBeaconResponse(c *jaxjson.GetBlockTemplateResult) (
	block *wire.MsgBlock, target *big.Int, height int64, err error) {

	// Block initialisation.
	height = c.Height

	beaconBlock := wire.EmptyBeaconBlock()
	block = &beaconBlock

	// Transactions processing.
	var reward, fee int64
	block.Transactions, reward, fee, err = unmarshalJaxTransactions(c.CoinbaseTxn, c.Transactions, true)
	if err != nil {
		return
	}

	if reward == 0 && c.CoinbaseValue != nil {
		reward = *c.CoinbaseValue
	}

	var coinbaseTx *jaxutil.Tx
	coinbaseTx, err = chaindata.CreateJaxCoinbaseTx(reward, fee, int32(height), 0,
		h.config.BtcMiningAddress, h.config.BurnBtcReward, true)
	if err != nil {
		return
	}
	block.Transactions[0] = coinbaseTx.MsgTx()

	// Block header processing.
	previousMMRRoot, err := chainhash.NewHashFromStr(c.PrevBlocksMMRRoot)
	if err != nil {
		return
	}
	previousBlockHash, err := chainhash.NewHashFromStr(c.PreviousHash)
	if err != nil {
		return
	}

	bits, err := unmarshalBits(c.Bits)
	if err != nil {
		return
	}

	targetBinary, err := hex.DecodeString(c.Target)
	target = (&big.Int{}).SetBytes(targetBinary)
	if err != nil {
		return
	}

	// Chain weight
	chainWeight, ok := new(big.Int).SetString(c.ChainWeight, 10)
	if !ok {
		err = errors.New("invalid chain weight")
		return
	}

	// Recalculate the merkle root with the updated extra nonce.
	uBlock := jaxutil.NewBlock(block)
	merkles := chaindata.BuildMerkleTreeStore(uBlock.Transactions(), false)

	block.Header = wire.NewBeaconBlockHeader(
		wire.BVersion(c.Version), int32(height), *previousMMRRoot, *previousBlockHash,
		*merkles[len(merkles)-1], fixtureMMProofHash, fixtureDateTime, bits, chainWeight, fixtureNonce)

	block.Header.BeaconHeader().SetShards(c.Shards)
	block.Header.BeaconHeader().SetK(c.K)
	block.Header.BeaconHeader().SetVoteK(c.VoteK)

	var rawAux []byte
	rawAux, err = hex.DecodeString(c.BTCAux)
	if err != nil {
		return
	}

	aux := wire.BTCBlockAux{}
	err = aux.Deserialize(bytes.NewBuffer(rawAux))
	if err != nil {
		return
	}

	block.Header.BeaconHeader().SetBTCAux(aux)
	return
}

func (h *Coordinator) decodeShardBlockTemplateResponse(c *jaxjson.GetBlockTemplateResult, shardID common.ShardID) (
	block *wire.MsgBlock, target *big.Int, height int64, err error) {

	if lastBCHeader == nil {
		// No beacon block candidate has been fetched yet -> no beacon header is available.
		// No way to generate SC block header, cause there is a dependency on a BC header.
		err = fmt.Errorf("can't initialise SC header: %w", e.ErrNoBCHeader)
		return
	}

	// Block initialisation.
	height = c.Height
	shardBlock := wire.EmptyShardBlock()
	block = &shardBlock

	// Transactions processing.
	var reward, fee int64
	block.Transactions, reward, fee, err = unmarshalJaxTransactions(c.CoinbaseTxn, c.Transactions, false)
	if err != nil {
		return
	}

	if reward == 0 && c.CoinbaseValue != nil {
		reward = *c.CoinbaseValue
	}

	bits, err := unmarshalBits(c.Bits)
	if err != nil {
		return
	}

	// Fix for shard block reward in mainnet
	if h.config.Network.Name == "mainnet" {
		reward = chaindata.CalcShardBlockSubsidy(3, bits, c.K)
		// logger.Log.Trace().Int64("reward in calc", reward).Uint32("bits", bits).Uint32("K", c.K).Msg("debug reward")
	}

	var coinbaseTx *jaxutil.Tx
	coinbaseTx, err = chaindata.CreateJaxCoinbaseTx(reward, fee,
		int32(height), uint32(shardID), h.config.BtcMiningAddress, !h.config.BurnBtcReward, false)
	if err != nil {
		return
	}
	block.Transactions[0] = coinbaseTx.MsgTx()

	// Block header processing.
	previousMMRRoot, err := chainhash.NewHashFromStr(c.PrevBlocksMMRRoot)
	if err != nil {
		return
	}
	PreviousHash, err := chainhash.NewHashFromStr(c.PreviousHash)
	if err != nil {
		return
	}

	targetBinary, err := hex.DecodeString(c.Target)
	target = (&big.Int{}).SetBytes(targetBinary)
	if err != nil {
		return
	}

	// Chain weight
	// Chain weight
	chainWeight, ok := new(big.Int).SetString(c.ChainWeight, 10)
	if !ok {
		err = errors.New("invalid shard chain weight")
		return
	}

	lastBCHeaderMutex.Lock()
	defer lastBCHeaderMutex.Unlock()

	// Recalculate the merkle root with the updated extra nonce.
	uBlock := jaxutil.NewBlock(block)
	merkles := chaindata.BuildMerkleTreeStore(uBlock.Transactions(), false)

	block.Header = wire.NewShardBlockHeader(
		int32(height), *previousMMRRoot, *PreviousHash, *merkles[len(merkles)-1], bits, chainWeight,
		*lastBCHeader, *lastBCCoinbaseAux.Copy())

	block.Header.BeaconHeader().SetShards(uint32(len(h.shards)))
	block.Header.BeaconHeader().SetK(c.K)
	block.Header.BeaconHeader().SetVoteK(c.VoteK)

	return
}

func (h *Coordinator) decodeBitcoinResponse(c *btcdjson.GetBlockTemplateResult) (
	block *btcdwire.MsgBlock, target *big.Int, height int64, err error) {

	// Block initialisation.
	height = c.Height

	bitcoinBlock := btcdwire.MsgBlock{}
	block = &bitcoinBlock

	// Transactions processing.
	var reward, fee int64
	block.Transactions, reward, fee, err = unmarshalBitcoinTransactions(c.CoinbaseTxn, c.Transactions)
	if err != nil {
		return
	}
	if len(block.Transactions) == 0 { // ToDO: Remove this part
		block.Transactions = make([]*btcdwire.MsgTx, 1)
	}

	block.Transactions = make([]*btcdwire.MsgTx, 1) // ToDo: Restore transactions

	if c.CoinbaseValue != nil {
		reward = *c.CoinbaseValue - fee
		// TODO: remove hardcode, use constant
		if reward > 6_2500_0000 {
			reward = 6_2500_0000
			fee = *c.CoinbaseValue - reward - fee // ToDo: Remove this, when restore transactions
		} else {
			fee = 0
		}
	} else {
		logger.Log.Error().Msg("Block reward is not defined !!!!")
	}

	// Different burn script according to config
	var burnScript []byte
	if h.config.EnableBCHMode {
		burnScript = types.RawBCHJaxBurnScript
	} else {
		burnScript = types.RawJaxBurnScript
	}

	var coinbaseTx *jaxutil.Tx
	coinbaseTx, err = chaindata.CreateJaxCoinbaseTxWithBurn(reward, fee, int32(height), 0,
		h.config.BtcMiningAddress, h.config.BurnBtcReward, false, burnScript)
	if err != nil {
		return
	}

	cTx := utils.JaxTxToBtcTx(coinbaseTx.MsgTx())
	block.Transactions[0] = &cTx

	witness := false
	var btcBlockTransactions []*btcdutil.Tx
	for _, msgTx := range block.Transactions {
		if msgTx.HasWitness() {
			witness = true
		}
		btcBlockTransactions = append(btcBlockTransactions, btcdutil.NewTx(msgTx))
	}

	if witness {
		btcdmining.AddWitnessCommitment(btcdutil.NewTx(&cTx), btcBlockTransactions)
	}

	// Block header processing.
	previousBlockHash, err := btcdchainhash.NewHashFromStr(c.PreviousHash)
	if err != nil {
		return
	}

	bits, err := unmarshalBits(c.Bits)
	if err != nil {
		return
	}

	targetBinary, err := hex.DecodeString(c.Target)
	target = (&big.Int{}).SetBytes(targetBinary)
	if err != nil {
		return
	}

	newBlock := btcdutil.NewBlock(block)
	merkles := blockchain.BuildMerkleTreeStore(newBlock.Transactions(), false)
	block.Header = *btcdwire.NewBlockHeader(c.Version, previousBlockHash, merkles[len(merkles)-1], bits, fixtureNonce)

	return
}

func unmarshalBitcoinTransactions(coinbaseTx *btcdjson.GetBlockTemplateResultTx,
	txs []btcdjson.GetBlockTemplateResultTx) (transactions []*btcdwire.MsgTx, reward, fee int64, err error) {

	unmarshalBitcoinTx := func(txHash string) (tx *btcdwire.MsgTx, err error) {
		txBinary, err := hex.DecodeString(txHash)
		if err != nil {
			return
		}

		tx = &btcdwire.MsgTx{}
		txReader := bytes.NewReader(txBinary)
		err = tx.Deserialize(txReader)
		return
	}

	transactions = make([]*btcdwire.MsgTx, 0, len(txs)+1)

	// Coinbase transaction must be processed first.
	// (transactions order in transactions slice is significant)
	if coinbaseTx != nil {
		cTX, err := unmarshalBitcoinTx(coinbaseTx.Data)
		if err != nil {
			return nil, 0, 0, err
		}
		for _, out := range cTX.TxOut {
			reward += out.Value
		}
		transactions = append(transactions, cTX)
	} else {
		// Coinbase transaction needs to be in any case
		transactions = append(transactions, &btcdwire.MsgTx{})
	}

	// Regular transactions processing.
	for _, marshalledTx := range txs {
		tx, err := unmarshalBitcoinTx(marshalledTx.Data)
		if err != nil {
			return nil, 0, 0, err
		}
		fee += marshalledTx.Fee
		transactions = append(transactions, tx)
	}

	return
}

func unmarshalJaxTransactions(coinbaseTx *jaxjson.GetBlockTemplateResultTx,
	txs []jaxjson.GetBlockTemplateResultTx, beacon bool) (transactions []*wire.MsgTx, reward, fee int64, err error) {

	unmarshalBeaconTx := func(txHash string) (tx *wire.MsgTx, err error) {
		txBinary, err := hex.DecodeString(txHash)
		if err != nil {
			return
		}

		tx = &wire.MsgTx{}
		txReader := bytes.NewReader(txBinary)
		err = tx.Deserialize(txReader)
		return
	}

	transactions = make([]*wire.MsgTx, 0, len(txs)+1)

	// Coinbase transaction must be processed first.
	// (transactions order in transactions slice is significant)
	if coinbaseTx != nil {
		cTX, err := unmarshalBeaconTx(coinbaseTx.Data)
		if err != nil {
			return nil, 0, 0, err
		}

		if beacon {
			reward += cTX.TxOut[1].Value
			reward += cTX.TxOut[2].Value
		} else {
			reward += cTX.TxOut[1].Value
		}

		transactions = append(transactions, cTX)
	} else {
		// Coinbase transaction needs to be in any case
		transactions = append(transactions, &wire.MsgTx{})
	}

	// Regular transactions processing.
	for _, marshalledTx := range txs {
		tx, err := unmarshalBeaconTx(marshalledTx.Data)
		if err != nil {
			return nil, 0, 0, err
		}
		fee += marshalledTx.Fee
		transactions = append(transactions, tx)
	}

	return
}

func unmarshalBits(hexBits string) (bits uint32, err error) {
	var val uint64
	val, err = strconv.ParseUint(hexBits, 16, 64)
	if err != nil {
		return
	}

	bits = uint32(val)
	return
}
