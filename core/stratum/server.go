package stratum

import (
	"net"
	"sync"
	"time"

	"gitlab.com/jaxnet/core/miner/core/communicator/events"
	"gitlab.com/jaxnet/core/miner/core/logger"
	"gitlab.com/jaxnet/core/miner/core/metrics"
	"gitlab.com/jaxnet/core/miner/core/miner/tasks"
	"gitlab.com/jaxnet/core/miner/core/settings"
	"gitlab.com/jaxnet/core/miner/core/state"
	"gitlab.com/jaxnet/core/miner/core/stratum/bans"
	"gitlab.com/jaxnet/core/miner/core/stratum/types"
	"gitlab.com/jaxnet/core/miner/core/stratum/vardiff"
	"gitlab.com/jaxnet/core/miner/core/utils"
)

// Server describes structure of the stratum server.
type Server struct {
	utils.StoppableMixin
	stopped             bool
	Config              *settings.Configuration
	Listener            net.Listener
	VarDiff             *vardiff.VarDiff
	StratumClients      map[uint64]*Client
	StratumClientsMutex sync.Mutex
	SubscriptionCounter *SubscriptionCounter
	BanningManager      *bans.BanningManager
	JobManager          *JobManager
	// rebroadcastTicker     *time.Ticker
	ReceiveMiningJobEvent   chan Job
	APIServer               *API
	StratumUpdateBlockEvent chan events.StratumUpdateBlock
}

func NewStratumServer(options *settings.Configuration) *Server {
	InitClientCtxPool()

	return &Server{
		Config:                options,
		BanningManager:        bans.NewBanningManager(options),
		SubscriptionCounter:   NewSubscriptionCounter(),
		StratumClients:        make(map[uint64]*Client),
		ReceiveMiningJobEvent: make(chan Job, 256),
		APIServer:             newAPIServer(options.Stratum.APIServer),
	}
}

// BroadcastJob broadcasts new jobs to stratum clients.
func (ss *Server) BroadcastJob() {
	go func() {
		for {
			currentJob, ok := <-ss.ReceiveMiningJobEvent
			if !ok {
				logger.Log.Trace().Msg("No job received to broadcast, sleeping...")
				time.Sleep(tasks.MiningRoundTimeWindow)
				continue
			}

			if currentJob.JobId != "" && !currentJob.Broadcasted {
				jobParams, err := ss.JobManager.GetJobParamsEx(ss.JobManager.CurrentJob, false)
				if err != nil {
					logger.Log.Error().Err(err).Msg("Failed to get job params")
					continue
				}

				ss.BroadcastCurrentMiningJob(jobParams)
				currentJob.Broadcasted = true
				metrics.IncCounterVec(metrics.CreatedJobs, nil)
			} else {
				logger.Log.Trace().Interface("job", currentJob).Msg("Debug current job")
			}
		}
	}()
}

func (ss *Server) HandleNewConnection() {
	go func() {
		for {
			conn, err := ss.Listener.Accept()
			if err != nil {
				ss.StratumClientsMutex.Lock()
				stratumClientsNumber := len(ss.StratumClients)
				ss.StratumClientsMutex.Unlock()

				logger.Log.Error().Err(err).Int("stratumClientsNumber", stratumClientsNumber).Msg("Failed to accept request")
				time.Sleep(time.Second * 30)
				continue
			}

			if conn != nil {
				logger.Log.Info().Str("from", conn.RemoteAddr().String()).Msg("New miner connection")

				// Increase connection deadline on each subsequent message
				deadline := time.Now().Add(time.Duration(ss.Config.Stratum.ConnectionTimeout) * time.Second)
				err = conn.SetDeadline(deadline)
				if err != nil {
					logger.Log.Error().Err(err).Str("from", conn.RemoteAddr().String()).Msg("Failed to increase connection deadline")
					continue
				} else {
					logger.Log.Trace().Str("from", conn.RemoteAddr().String()).Int64("deadline", deadline.Unix()).Msg("Increased connection deadline")
				}

				go ss.HandleNewClient(conn)
			}
		}
	}()
}

// // PublishPeriodicMetrics produces periodic metrics.
// func (ss *Server) PublishPeriodicMetrics() {
// 	go func() {
// 		statsTicker := time.NewTicker(1 * time.Minute)
// 		defer statsTicker.Stop()
//
// 		for {
// 			select {
// 			case <-statsTicker.C:
// 				ss.StratumClientsMutex.Lock()
// 				for id := range ss.StratumClients {
// 					workerID := strconv.Itoa(int(ss.StratumClients[id].ExtraNonce1))
//
// 					metrics.SetGaugeVec(metrics.SubmittedShares,
// 						map[string]string{
// 							"worker":       ss.StratumClients[id].WorkerName,
// 							"valid_shares": "true",
// 							"id":           workerID,
// 						},
// 						float64(ss.StratumClients[id].Metrics.SharesPerMinute.GetValid()),
// 					)
//
// 					metrics.SetGaugeVec(metrics.SubmittedShares,
// 						map[string]string{
// 							"worker":       ss.StratumClients[id].WorkerName,
// 							"valid_shares": "false",
// 							"id":           workerID,
// 						},
// 						float64(ss.StratumClients[id].Metrics.SharesPerMinute.GetInvalid()),
// 					)
//
// 					metrics.SetGaugeVec(metrics.HashRate,
// 						map[string]string{
// 							"worker": ss.StratumClients[id].WorkerName,
// 							"id":     workerID,
// 						},
// 						float64(ss.StratumClients[id].varDiff.CalcHashRate()),
// 					)
//
// 					logger.Log.Info().Str("worker", ss.StratumClients[id].WorkerName).
// 						Uint64("last_minute_valid", ss.StratumClients[id].Metrics.SharesPerMinute.GetValid()).
// 						Uint64("last_minute_invalid", ss.StratumClients[id].Metrics.SharesPerMinute.GetInvalid()).
// 						Uint64("total_valid", ss.StratumClients[id].Metrics.Shares.GetValid()).
// 						Uint64("total_invalid", ss.StratumClients[id].Metrics.Shares.GetInvalid()).
// 						Str("hash_rate", ss.StratumClients[id].CalcHashRate()).
// 						Msg("Submitted shares by worker")
//
// 					// ss.StratumClients[id].Metrics.SharesPerMinute.Reset()
// 				}
// 				ss.StratumClientsMutex.Unlock()
// 			}
// 		}
// 	}()
// }

func (ss *Server) Init(minerResultChan chan tasks.MinerResult, stratumUpdateBlockEvent chan events.StratumUpdateBlock) {
	if ss.Config.Stratum.Banning.Enabled {
		ss.BanningManager.Init()
	}

	ss.JobManager = NewJobManager(ss.Config, minerResultChan)

	// Get channel from communicator and pass to stratum client to handle mining.update_block method
	ss.StratumUpdateBlockEvent = stratumUpdateBlockEvent

	var err error
	if ss.Config.Stratum.Ports.TLS {
		// TODO: fix
		// ss.Listener, err = tls.Listen("tcp", ":"+strconv.Itoa(port), options.TLS.ToTLSConfig())
	} else {
		// ss.Listener, err = net.Listen("tcp", ":"+strconv.Itoa(port))
		ss.Listener, err = net.Listen("tcp4", ":"+ss.Config.Stratum.Ports.Port)
	}

	if err != nil {
		logger.Log.Panic().Err(err).Msg("Failed to start stratum server")
	} else {
		logger.Log.Info().Str("port", ss.Config.Stratum.Ports.Port).Msg("Stratum server started")
	}

	// Runs internal API
	ss.RunAPI()

	// Broadcast new jobs
	ss.BroadcastJob()

	// New connection handling
	ss.HandleNewConnection()
}

// HandleClientDisconnect handles stratum client disconnect.
func (ss *Server) HandleClientDisconnect(client *Client) {
	<-client.SocketClosedEvent
	client.Disconnect()
	ss.RemoveStratumClientBySubscriptionId(client.SubscriptionId)
}

// HandleNewClient converts the connection to an underlying client instance
func (ss *Server) HandleNewClient(socket net.Conn) {
	// New stratum client connected
	ss.StratumClientsMutex.Lock()
	subscriptionID := ss.SubscriptionCounter.Next()
	client := NewStratumClient(subscriptionID, socket, ss.Config, ss.BanningManager, ss.JobManager, ss.ReceiveMiningJobEvent, ss.StratumUpdateBlockEvent)
	ss.StratumClients[subscriptionID] = client
	ss.StratumClientsMutex.Unlock()

	client.Init()

	// Handle stratum client disconnect
	go ss.HandleClientDisconnect(client)
}

func (ss *Server) BroadcastCurrentMiningJob(jobParams types.JobParams) {
	ss.StratumClientsMutex.Lock()
	for clientId := range ss.StratumClients {
		go ss.StratumClients[clientId].SendMiningJob(jobParams)
	}
	ss.StratumClientsMutex.Unlock()
}

func (ss *Server) RemoveStratumClientBySubscriptionId(subscriptionId uint64) {
	ss.StratumClientsMutex.Lock()
	delete(ss.StratumClients, subscriptionId)
	ss.StratumClientsMutex.Unlock()
}

// Run sends current mining task to miner.
func (ss *Server) Run(c *state.Coordinator) {
	var (
		currentTask *tasks.MinerTask
		nextTask    *tasks.MinerTask
	)

	for {
		if ss.MustBeStopped {
			ss.stopped = true
			return
		}

		nextTask = c.NextTask()
		if nextTask == nil && currentTask == nil {
			// There is no work available for miner.
			// Even if current task is not nil - no mining must be done,
			// because the actual BC header has been set to nil on the ctx's side.
			//
			// Wait some time and check again later.
			time.Sleep(tasks.MiningRoundTimeWindow)
			continue
		}

		if currentTask == nil || currentTask != nextTask {
			// The task has been updated, so the mining context must be reset.
			// In all other cases the mining would be continued with previous context.

			// Possible optimisation when building stratum job.
			// Wait until bitcoin, beacon and shard block candidates are fetched
			if nextTask.BlockFlags&(tasks.BitcoinFlag|tasks.BeaconFlag) == nextTask.BlockFlags {
				ss.JobManager.CurrentJob = *NewJob(nextTask)

				currentTask = nextTask
				logger.Log.Trace().Str("jobId", ss.JobManager.CurrentJob.JobId).Msg("New stratum job")
				// TODO: maybe send task via channel only, not just a notification about new task?
				ss.ReceiveMiningJobEvent <- ss.JobManager.CurrentJob
			} else {
				logger.Log.Trace().Uint8("block flags", uint8(nextTask.BlockFlags)).Int("shards number", len(nextTask.ShardsTargets)).Msg("nextTask is not fully populated with data")
				time.Sleep(tasks.MiningRoundTimeWindow)
			}
		} else {
			// No block mined in tasks.MiningRoundTimeWindow, reset current task to generate new job
			currentTask = nil
			time.Sleep(tasks.MiningRoundTimeWindow)
		}
	}
}

// RunAPI runs internal API server within Stratum server
func (ss *Server) RunAPI() {
	go ss.APIServer.Run()
}

// StopAPI stops internal API server within Stratum server
func (ss *Server) StopAPI() {
	// TODO: implement
}
