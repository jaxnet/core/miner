/*
 * Copyright (c) 2020 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package main

import (
	"context"
	"net/http"
	_ "net/http/pprof"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	c "gitlab.com/jaxnet/core/miner/core/communicator"
	"gitlab.com/jaxnet/core/miner/core/e"
	"gitlab.com/jaxnet/core/miner/core/logger"
	"gitlab.com/jaxnet/core/miner/core/metrics"
	"gitlab.com/jaxnet/core/miner/core/settings"
	s "gitlab.com/jaxnet/core/miner/core/state"
	"gitlab.com/jaxnet/core/miner/core/storage"
	ss "gitlab.com/jaxnet/core/miner/core/stratum"
)

var (
	coordinator  *s.Coordinator
	communicator *c.Communicator
	stratum      *ss.Server
)

func main() {
	observer, err := settings.NewObserver()
	e.AbortOn(err)

	logger.Init(observer.Settings().Log)
	stopSignal := interruptListener(logger.Log)

	ctx, cancel := context.WithCancel(context.Background())

	go observer.ObserveConfigurationChanges()

	for {
		select {
		case conf := <-observer.ConfigurationsFlow():
			synchronouslyStopAllComponents()
			reinitialiseWithConfiguration(ctx, conf)
		case <-stopSignal:
			cancel()

			done := make(chan struct{})

			go func() {
				synchronouslyStopAllComponents()
				done <- struct{}{}
			}()

			select {
			case <-time.NewTimer(15 * time.Second).C:
			case <-done:
				if err := storage.Stop(); err != nil {
					logger.Log.Error().Err(err).Msg("Failed to gracefully stop storage")
				}
			}

			return
		}
	}
}

func reinitialiseWithConfiguration(ctx context.Context, conf *settings.Configuration) {
	coordinator = s.New(ctx, conf)
	communicator = c.New(ctx, conf)

	// Storage singleton
	err := storage.New(conf)
	if err != nil {
		e.AbortOn(err)
	}

	// Stratum server
	if conf.Stratum.APIServer.EnableMetrics {
		metrics.Init()
	}

	stratum = ss.NewStratumServer(conf)
	stratum.Init(communicator.MinerResults(), communicator.StratumUpdateBlock)
	go stratum.Run(coordinator)

	// Coordinator
	go coordinator.RunUsing(
		communicator.ShardsBlockCandidates(),
		communicator.BeaconBlockCandidates(),
		communicator.BitcoinBlockCandidates(),
	)

	// Communicator
	go communicator.Run()

	if conf.EnablePprof {
		go func() {
			http.ListenAndServe("localhost:6060", nil)
		}()
	}
}

func synchronouslyStopAllComponents() {
	group := &sync.WaitGroup{}

	if coordinator != nil {
		group.Add(1)
		go coordinator.StopUsing(group)
	}

	if communicator != nil {
		group.Add(1)
		go communicator.StopUsing(group)
	}

	group.Wait()
}

// interruptListener listens for OS Signals such as SIGINT (Ctrl+C) and shutdown
// requests from shutdownRequestChannel.  It returns a channel that is closed
// when either signal is received.
func interruptListener(log *logger.Logger) <-chan struct{} {
	var interruptSignals = []os.Signal{syscall.SIGINT, syscall.SIGTERM}

	done := make(chan struct{})
	go func() {
		interruptChannel := make(chan os.Signal, 1)
		signal.Notify(interruptChannel, interruptSignals...)

		// Listen for initial shutdown signal and close the returned
		// channel to notify the caller.
		select {
		case sig := <-interruptChannel:
			log.Info().Msg("Received signal " + sig.String() + ". Shutting down...")

		}
		close(done)

		// Listen for repeated signals and display a message so the user
		// knows the shutdown is in progress and the process is not
		// hung.
		for {
			select {
			case sig := <-interruptChannel:
				log.Info().Msg("Received signal " + sig.String() + ". Already shutting down...")
			}
		}
	}()

	return done
}
