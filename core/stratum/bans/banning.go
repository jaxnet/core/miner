package bans

import (
	"sync"
	"time"

	"gitlab.com/jaxnet/core/miner/core/logger"
	"gitlab.com/jaxnet/core/miner/core/settings"
)

type BanningManager struct {
	Enabled           bool
	Config            *settings.Configuration
	BannedIPList      map[string]*time.Time
	BannedIPListMutex sync.Mutex
}

func NewBanningManager(options *settings.Configuration) *BanningManager {
	return &BanningManager{
		Enabled:      options.Stratum.Banning.Enabled,
		Config:       options,
		BannedIPList: make(map[string]*time.Time),
	}
}

func (bm *BanningManager) Init() {
	if !bm.Enabled {
		return
	}

	go func() {
		ticker := time.NewTicker(time.Duration(bm.Config.Stratum.Banning.PurgeInterval) * time.Second)
		defer ticker.Stop()

		for {
			<-ticker.C
			bm.BannedIPListMutex.Lock()
			for ip, banTime := range bm.BannedIPList {
				if time.Since(*banTime) > time.Duration(bm.Config.Stratum.Banning.Time)*time.Second {
					delete(bm.BannedIPList, ip)
				}
			}
			bm.BannedIPListMutex.Unlock()
		}
	}()
}

func (bm *BanningManager) CheckBan(strRemoteAddr string) (shouldCloseSocket bool) {
	if !bm.Enabled {
		return
	}

	if bm.BannedIPList[strRemoteAddr] != nil {
		bm.BannedIPListMutex.Lock()
		bannedTime := bm.BannedIPList[strRemoteAddr]
		bannedTimeAgo := time.Since(*bannedTime)
		timeLeft := time.Duration(bm.Config.Stratum.Banning.Time)*time.Second - bannedTimeAgo
		if timeLeft > 0 {
			logger.Log.Info().Str("remote_addr", strRemoteAddr).Msg("Worker is banned")
			bm.BannedIPListMutex.Unlock()
			return true
			// client.Socket.Close()
			// kickedBannedIP
		} else {
			delete(bm.BannedIPList, strRemoteAddr)
			bm.BannedIPListMutex.Unlock()
			// forgaveBannedIP
		}
	}

	return false
}

func (bm *BanningManager) AddBannedIP(strRemoteAddr string) {
	if !bm.Enabled {
		return
	}

	now := time.Now()
	bm.BannedIPListMutex.Lock()
	bm.BannedIPList[strRemoteAddr] = &now
	bm.BannedIPListMutex.Unlock()
}
