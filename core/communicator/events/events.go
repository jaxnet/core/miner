/*
 * Copyright (c) 2020 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package events

import (
	btcdjson "github.com/btcsuite/btcd/btcjson"

	"gitlab.com/jaxnet/core/miner/core/common"
	"gitlab.com/jaxnet/jaxnetd/types/jaxjson"
)

type ShardBlockCandidate struct {
	ShardID   common.ShardID
	Candidate *jaxjson.GetBlockTemplateResult
}

type BeaconBlockCandidate struct {
	Candidate *jaxjson.GetBlockTemplateResult
}

type BitcoinBlockCandidate struct {
	Candidate *btcdjson.GetBlockTemplateResult
}

// StratumUpdateBlock handles stratum server mining.update_block method and triggers fetching next block candidate
// for a specified chain.
// -1 for Bitcoin, 0 for Beacon and 1 and more for shard chains.
type StratumUpdateBlock struct {
	ChainID int64
}
