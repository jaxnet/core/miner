# Jax stratum server implementation

This document describes a protocol, that allows a group of miners to connect
to a server, which coordinates the distribution of work packages among
miners.

The initial protocol was written for Bitcoin and contains several pieces that
need adjustment in order to be usable with Aeternity.

Pool operators looking to implement this specification should note, that the
usage of the Bitcoin-NG protocol changes the dynamics of the mining game.
With Bitcoin-NG pools will end up authoring the microblocks containing
transactions while the miners try to find keyblocks, which are used for
leader election.
This procedure puts both more power into the hands of pool operators while
also adding more burden. This burden comes in the form of increased book
keeping complexity—the full revenue of one epoch can only be computed after
the fact—and an increase in computational resources required for signing
microblocks.


## Specification


### Conventions

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD",
"SHOULD NOT", "RECOMMENDED",  "MAY", and "OPTIONAL" in this document are to
be interpreted as described in [RFC 2119](https://tools.ietf.org/html/rfc2119).

The stratum protocol mostly adheres to the [JSON RPC 2.0](https://www.jsonrpc.org/specification) specification.

This specification derives from works by [slushpool](https://slushpool.com/help/manual/stratum-protocol),
[zcash](https://github.com/str4d/zips/blob/77-zip-stratum/drafts/str4d-stratum/draft1.rst)
and [nicehash](https://github.com/nicehash/Specifications/blob/master/EthereumStratum_NiceHash_v1.0.0.txt).


### Overview

Communication happens over a bidirectional channel with messages encoded as
JSON with `LF` delimiter—in this document written as `\n`.

Since requests can be handled out of order, each request in a session SHOULD
have an unique `id`. In order to be able to match request and response,
responses MUST have the same `id` as their matching request. Notifications
sent by the server or calls that do not trigger any response MAY have an `id` of
`null`.

For further details on the members of request and response objects consult the
[JSON RPC 2.0 specification](https://www.jsonrpc.org/specification).


### Protocol flow example

The following shows what a session might look like from subscription to
submitting a solution.

```
Client                                Server
  |                                     |
  | --------- mining.configure -------> |
  | --------- mining.subscribe -------> |
  | --------- mining.authorize -------> |
  | <-------- mining.set_target ------- |
  | <-------- mining.set_difficulty --- |
  |                                     |----
  | <---------- mining.notify --------- |<--/
  |                                     |
  | ---------- mining.submit ---------> |
```


### Methods

- [mining.configure](#miningconfigure)
- [mining.subscribe](#miningsubscribe)
- [mining.authorize](#miningauthorize)
- [mining.set_difficulty](#miningset_difficulty)
- [mining.notify](#miningnotify)
- [mining.set_target](#miningset_target)
- [mining.submit](#miningsubmit)
- [client.reconnect](#clientreconnect)


### Errors

Whenever an RPC call triggers an error, the response MUST include an `error`
field which maps to a **list** of the following values:

- [ `code` : `int` ]
- [ `message` : `string` ]
- [ `data` : `object` ]

```
{"id": 10, "result": null, "error": [21, "Job not found", null]}\n
```

Errors SHOULD be identified by their `code` and programs SHOULD do error
handling based on the `code` and not the `message`.
Available error codes, in addition to the codes defined in the [JSON RPC 2.0]() specification, are:

- `20` - Other/Unknown
- `21` - Job not found (=stale)
- `22` - Duplicate share
- `23` - Low difficulty share
- `24` - Unauthorized worker
- `25` - Not subscribed

The `message` field SHOULD be a concise description of the error for human
consumption.

Implementors MAY choose to include an optional `data` object with additional
information relevant to the error.


### mining.configure

In order to allow easier upgrades in the future, a client MAY send a
configuration as its initial messages to the server, specifying possible
extensions it supports.
The specific mechanism is described by [slushpool here](https://github.com/slushpool/stratumprotocol/blob/master/stratum-extensions.mediawiki).

A client MAY choose to skip this message, which a server SHOULD interpret as the
client adhering to the protocol as described in this document, without any
further extensions.

This method call will only be executed by clients.

This stratum implementation supports ASIC boost AKA version rolling feature.
White paper for this feature [here](https://arxiv.org/pdf/1604.00575.pdf%E3%80%82).
Link to this particular extension [here](https://github.com/slushpool/stratumprotocol/blob/master/stratum-extensions.mediawiki#extension-version-rolling).
Useful link of implementation in asic firmware [here](https://github.com/braiins/braiins/blob/bos-devel/open/protocols/stratum/src/test_utils/v1.rs).
Article by Braiins about this extension is [here](https://braiins-systems.medium.com/bitmain-antminer-s9-asicboost-capability-verified-by-braiins-13-in-energy-savings-and-planned-da67c290152c).

#### Request:

Example of a message from client supporting this extension.
```
{"id":12345, "method":"mining.configure", "params":[["version-rolling"],{"version-rolling.mask":"1fffe000","version-rolling.min-bit-count":3}]}\n
```

#### Response

Example of a message from server supporting this extension.
```
{"id":12345,"error":null,"result":{"version-rolling":true,"version-rolling.mask":"1fffe000"}}\n
```


### mining.subscribe

In order to initiate or resume a session with the server, a client needs to
call the subscribe method.

This method call will only be executed by clients.


#### Request:

```
{"id": 12345, "method": "mining.subscribe", "params": ["Antminer S19/Sun Apr 11 20:36:08 CST 2021", "1"]}\n
```

- [ `id` : `int` ]: request id
- [ `method` : `string` ]: RPC method name
- [ `params` : (`string`, `string`) ]: list of method parameters
    1. MUST be name and version of mining software in the given format or empty
       string
    2. The optional second parameter specifies a `mining.notify` subscription id the client 
       wishes to resume working with (possibly due to a dropped connection). 
       If provided, a server MAY (at its option) issue the connection the same `extranonce1`. 
       Note that the `extranonce1` may be the same (allowing a resumed connection) even 
       if the subscription id is changed!


#### Response

```
{"id":12345,"result":[[["mining.set_difficulty","1"],["mining.notify","1"]],"369b2f7e",4]}\n
```

- [ `id` : `int` ]: request id
- [ `result` : (( ("mining.set_difficulty",`string`),("mining.notify",`string`),`string`,`int`) )) ]:
    - MUST be `null` if an error occurred or otherwise
        1. Subscriptions - An array of 2-item tuples, each with a subscription type and id.
        2. ExtraNonce1. - Hex-encoded, per-connection unique string which will be used for creating generation transactions later.
        3. ExtraNonce2_size - The number of bytes that the miner users for its ExtraNonce2 counter.
- [ `error` : (`int`, `string`, `object`) ]

This nonce sent by the server is usually referred to as the extranonce, i.e.
`nonce = minernonce || extranonce`. The server MAY freely choose the length of
the extranonce and clients SHOULD NOT expect it to be always of the same length.
`extranonce` and `minernonce` MUST be 8 bytes long together.

The nonce is in little-endian form.

The server SHOULD assign disjoint subspaces of the 8 byte nonce space to miners,
avoiding the problem of multiple miners working on the same problem.


### mining.authorize

Before a client can submit solutions to a server it MUST authorize at least one worker.

This method call will only be executed by clients.
Server can have custom authorization method.
For example validate worker name as bitcoin address.
Only authorized clients receive mining jobs.

#### Request

```
{"id": 2, "method": "mining.authorize", "params": ["WORKER_NAME", "WORKER_PASSWORD"]}\n
```

- [ `id` : `int` ]: request id
- [ `method` : `string` ]: RPC method name
- [ `params` : (`string`, `string`) ]: list of method parameters
    1. The worker name
    2. The worker password


#### Response

```
{"id": 2, "result": true, "error": null}\n
```

- [ `id` : `int` ]: request id
- [ `result` : `bool` ]: authorization success
    - MUST be `true` if successful
    - MUST be `null` if an error occurred
- [ `error` : (`int`, `string`, `object`) ]
    - MUST be `null` if `result` is `true`
    - If authorization failed then it MUST contain error object with the
      appropriate error id and description



#### Request with password in form

```
{"id":55465,"method":"mining.authorize","params":["204645","dmin=226000,dstart=226000,dmax=1500000"]}\n
```

Enables variable difficulty feature of stratum server.
This feature allows changing difficulty for the miner to obtain maximum effectiveness of mining.
It calculates shares submitted by particular miner and adjusts difficulty 
with [mining.set_difficulty](#miningset_difficulty) method to remain in a certain range configured in
config file.
Example of such options:

```yaml
var_diff:
  start_diff: 65536
  min_diff: 16384
  max_diff: 131072
  target_shares_per_time_window: 10
  target_shares_per_time_window_threshold: 3
  time_window: 60
  retarget_time_window: 60
  enabled: true
```

In this case miner receives command to start mining with difficulty 65536.
Then stratum starts to count received shares from miner within `time_window` time
expressed in seconds. Every `retarget_time_window` it checks if received shares within
`[target_shares_per_time_window-target_shares_per_time_window_threshold..target_shares_per_time_window-target_shares_per_time_window_threshold]`
range, for this example it equals to  `[10-3..10+3]`.
In other word server expected from 7 to 13 shares per 60 seconds from miner.
If this not a case then it proportionally decreased or increases difficulty within
`[min_diff..max_diff]` range to achieve desired shares count from miner.


### mining.set_difficulty

The target difficulty for a block can change, and a server needs to be able to
notify clients of that. This method sends to the client block target converted to
relative bitcoin block difficulty.

This method call will only be executed by the server.

The highest possible target (difficulty 1) is defined as 0x1d00ffff, which gives us a hex target of
`0x00000000FFFF0000000000000000000000000000000000000000000000000000`.


#### Request

```
{"id": null, "method": "mining.set_difficulty", "params": [1]}\n
```

- [ `id` : `int` ]: request id
- [ `method` : `string` ]: RPC method name
- [ `params` : (`string`) ]: list of method parameters
    1. The the 256bit big-endian target, which MUST be hex encoded.

Any subsequent jobs started by a client after receiving this update MUST
honor the new target and servers MUST reject submission above this target.

For jobs started before this update, a server MAY accept submissions if they are
below the previous target.

Please consult the document describing the consensus and proof-of-work
algorithms for details on the target.


#### Response

There is no explicit response for this call.


### mining.set_target

The target difficulty for a block can change, and a server needs to be able to
notify clients of that.

This method call will only be executed by the server.


#### Request

```
{"id": null, "method": "mining.set_target", "params": ["FFFF000000000000000000000000000000000000000000000000000000000000"]}\n
```

- [ `id` : `int` ]: request id
- [ `method` : `string` ]: RPC method name
- [ `params` : (`string`) ]: list of method parameters
    1. The the 256bit big-endian target, which MUST be hex encoded.

Any subsequent jobs started by a client after receiving this update MUST
honor the new target and servers MUST reject submission above this target.

For jobs started before this update, a server MAY accept submissions if they are
below the previous target.

Please consult the document describing the consensus and proof-of-work
algorithms for details on the target.


#### Response

There is no explicit response for this call.


### mining.notify

The notify call is used to supply a worker with new work packages.

This method call will only be executed by the server.


#### Request

```
{"id":0,"method":"mining.notify","params":["73cb71a52b3cd65e","fb3a66b3ea5...993baeacb03c87f850000000000000000","01000000010000...","066a61786e6...c00000000",[],"20000000","1804bce4","624abb98",false]}
```

- [ `id` : `int` ]: request id
- [ `method` : `string` ]: RPC method name
- [ `params` : (`string`, `string`, `string`, `string`, [`string1`,...,`stringN`], `string`, `string`, `string`, `bool`) ]: list of method parameters
  1. Job ID. This is included when miners submit a results so work can be matched with proper transactions.
  2. Hash of previous block. Used to build the header.
  3. Generation transaction (part 1). The miner inserts ExtraNonce1 and ExtraNonce2 after this section of the transaction data.
  4. Generation transaction (part 2). The miner appends this after the first part of the transaction data, and the two ExtraNonce values.
  5. List of merkle branches. The generation transaction is hashed against the merkle branches to build the final merkle root.
  6. Bitcoin block version. Used in the block header.
  7. nBits. The encoded network difficulty. Used in the block header.
  8. nTime. The current time. nTime rolling should be supported, but should not increase faster than actual time.
  9. Clean Jobs. If true, miners should abort their current work and immediately use the new job. If false, they can still use the current job, but should move to the new one after exhausting the current nonce range.

In order to keep the protocol flexible and anticipate changes in both the
structure of blocks and proof of work scheme, the block version should be
considered as a switch for the subsequent parameters.

The following is valid for block version (***TODO:*** insert genesis block version).

In its current iteration, all that is needed as input for the proof of work
puzzle solver is a hash of the block header plus a nonce, as described in the
consensus document.
Thus a client only requires the header hash from the server.


#### Response

There is no explicit response for this call.


### mining.submit

With this method a worker can submit solutions for the mining puzzle.
This method call will only be executed by clients.


#### Request

```
{"id": 4, "method": "mining.submit", "params": ["WORKER_NAME", "d70fd222", "98b6ac44d2", ""]}\n
{"id": 3, "method": "mining.submit", "params":["worker1","f8d6ff6e4042e108","94060000","6246b63c","53bfa246","00600000"]}\n
```

- [ `id` : `int` ]: request id
- [ `method` : `string` ]: RPC method name
- [ `params` : (`string`, `string`, `string`, `string`, `string`, `string`) ]: list of method parameters
  1. Worker Name.
  2. Job ID.
  3. ExtraNonce2.
  4. nTime.
  5. nOnce.
  6. Version bits mask, optional,
     non empty for hardware supporting [version-rolling extension](https://github.com/slushpool/stratumprotocol/blob/master/stratum-extensions.mediawiki#extension-version-rolling).

As with `mining.notify` all parameters after the job ID should be understood as
being conditional on the block version sent along in `mining.notify`.

The miner nonce MUST be `8 - len(extra_nonce)` bytes long and be little endian,
in order for the server to produce a correct 8 bytes nonce when concatenating
the extra and miner nonce.


#### Response

```
{"id": 4, "result": true, "error": null}\n
```

- [ `id` : `int` ]: request id
- [ `result`: `bool` ]: submission accepted
    - MUST be `true` if accepted
    - MUST be `null` if an error occurred
- [ `error` : (`int`, `string`, `object`) ]
    - MUST be `null` if `result` is `true`
    - If submission failed then it MUST contain error object with the
      appropriate error id and description


### client.reconnect (NOT IMPLEMENTED)

If a pool operator wants to have their clients reconnect to the same or a
different host they use this call.

This method call will only be executed by the server.


#### Request

```
{"id": null, "method": "client.reconnect", "params": [("my.pool.com", 1234, 23)]} \n
```


- [ `id` : `int` ]: request id
- [ `method` : `string` ]: RPC method name
- [ `params`: (`string`, `int`, `int`) ]: list of method parameters
    1. MUST be the new host name or `null` if the name is the same
    2. MUST be the new port name or `null` if the port is the same
    3. MUST be a positive integer wait time in seconds that the client should wait

The `params` list MAY be empty, which signals that the client SHOULD reconnect
to the same host and port immediately.


#### Response

There is no explicit response for this call.



### Copyright

Copied from [https://github.com/aeternity/protocol](https://github.com/aeternity/protocol/blob/master/STRATUM.md)
