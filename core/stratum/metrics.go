package stratum

import "sync/atomic"

func (m *Metrics) SetHashRate(hr uint64) {
	atomic.StoreUint64(&m.HashRate, hr)
}

func (m *Metrics) GetHashRate() uint64 {
	return atomic.LoadUint64(&m.HashRate)
}

func (m *Metrics) ResetHashRate() {
	atomic.StoreUint64(&m.HashRate, 0)
}
