package stratum

import "sync/atomic"

type Shares struct {
	Valid   uint64
	Invalid uint64
}

func (s *Shares) TotalShares() uint64 {
	return atomic.LoadUint64(&s.Valid) + atomic.LoadUint64(&s.Invalid)
}

func (s *Shares) BadPercent() float64 {
	return float64(atomic.LoadUint64(&s.Invalid)*100) / float64(s.TotalShares())
}

func (s *Shares) Reset() {
	atomic.StoreUint64(&s.Invalid, 0)
	atomic.StoreUint64(&s.Valid, 0)
}

func (s *Shares) IncrementValid() {
	atomic.AddUint64(&s.Valid, 1)
}

func (s *Shares) GetValid() uint64 {
	return atomic.LoadUint64(&s.Valid)
}

func (s *Shares) IncrementInvalid() {
	atomic.AddUint64(&s.Invalid, 1)
}

func (s *Shares) GetInvalid() uint64 {
	return atomic.LoadUint64(&s.Invalid)
}
