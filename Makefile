build_docker:
	@docker build . -t jax-stratum:master

run_docker: build_docker
	@docker run --rm --name jax-stratum --mount type=bind,source="$(pwd)",target=/jax-stratum jax-stratum:master

build:
	@go build -o stratum

run: build
	@docker-compose -f docker-compose.yaml up -d
	@./stratum

build_linux:
	@GOOS=linux GOARCH=amd64 go build -o stratum.linux

run_linux: build_linux
	@./stratum-linux

