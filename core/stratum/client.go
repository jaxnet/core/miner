package stratum

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"math"
	"math/big"
	"net"
	"os"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/davecgh/go-spew/spew"
	"github.com/google/uuid"
	"github.com/patrickmn/go-cache"
	"gitlab.com/jaxnet/core/miner/core/communicator/events"
	"gitlab.com/jaxnet/core/miner/core/logger"
	"gitlab.com/jaxnet/core/miner/core/metrics"
	"gitlab.com/jaxnet/core/miner/core/settings"
	"gitlab.com/jaxnet/core/miner/core/stratum/bans"
	"gitlab.com/jaxnet/core/miner/core/stratum/types"
	"gitlab.com/jaxnet/core/miner/core/stratum/vardiff"
	"gitlab.com/jaxnet/core/miner/core/utils"
	"gitlab.com/jaxnet/jaxnetd/types/pow"
)

// 10 plus 1 minutes
const avgBitcoinBlockMiningIntervalExpiration = 10 + 1

// Purge 5 minutes after expiration
const avgBitcoinBlockMiningIntervalPurge = avgBitcoinBlockMiningIntervalExpiration + 5

// Version rolling constants
const versionRollingServerMask = uint32(0x1fffe000)

// Max message size/buffer size in bytes
const maxMessageSize = 4 * 1024

// Minimum pool difficulty, subject to change sometime
const minPoolDifficulty = 0.001

var clientCtxPool *cache.Cache

func InitClientCtxPool() {
	clientCtxPool = cache.New(avgBitcoinBlockMiningIntervalExpiration*time.Minute, avgBitcoinBlockMiningIntervalPurge*time.Minute)
}

// clientConnCtx is used to restore client connection options.
// Used by mining.subscribe method.
type clientConnCtx struct {
	workerName    string
	extraNonce1   uint32
	clientOptions map[string]float64
}

// Activity represents data to track worker (re)connects
type Activity struct {
	Connected   int64
	Reconnected int64
}

type Metrics struct {
	Shares          *Shares // Total shares submitted by worker
	SharesPerMinute *Shares // Shares per minute submitted by worker
	HashRate        uint64  // Current hash rate of client
	Activity        *Activity
}

type Client struct {
	SubscriptionId uint64
	Options        *settings.Configuration
	RemoteAddress  net.Addr

	Socket           net.Conn
	SocketBufIO      *bufio.ReadWriter
	SocketBufIOMutex sync.Mutex

	LastActivity time.Time
	Metrics      *Metrics

	IsAuthorized           bool
	SubscriptionBeforeAuth bool

	ExtraNonce1 uint32

	varDiff *vardiff.VarDiff

	WorkerName string
	WorkerPass string

	// TODO: used by ManuallySetValues method. Delete?
	CurrentTarget  uint32
	PreviousTarget uint32

	PendingDifficulty  *big.Float
	CurrentDifficulty  *big.Float
	PreviousDifficulty *big.Float
	ClientTarget       *big.Int

	JobManager                  *JobManager
	BanningManager              *bans.BanningManager
	SocketClosedEvent           chan struct{}
	SendMiningJobEvent          chan Job
	sendStratumUpdateBlockEvent chan events.StratumUpdateBlock

	// Version rolling support
	versionRolling types.VersionRolling
}

func NewStratumClient(subscriptionId uint64, socket net.Conn, options *settings.Configuration, bm *bans.BanningManager, jm *JobManager, sendMiningJobEvent chan Job, stratumUpdateBlock chan events.StratumUpdateBlock) *Client {
	return &Client{
		SubscriptionId:              subscriptionId,
		Options:                     options,
		RemoteAddress:               socket.RemoteAddr(),
		Socket:                      socket,
		SocketBufIO:                 bufio.NewReadWriter(bufio.NewReaderSize(socket, maxMessageSize), bufio.NewWriterSize(socket, maxMessageSize)),
		LastActivity:                time.Now(),
		IsAuthorized:                false,
		SubscriptionBeforeAuth:      false,
		varDiff:                     vardiff.NewVarDiff(options),
		BanningManager:              bm,
		JobManager:                  jm,
		Metrics:                     newMetrics(),
		ExtraNonce1:                 utils.RandUint32(),
		SocketClosedEvent:           make(chan struct{}),
		SendMiningJobEvent:          sendMiningJobEvent,
		sendStratumUpdateBlockEvent: stratumUpdateBlock,
	}
}

func (sc *Client) Init() {
	sc.SetupSocket()
	sc.VarDiffStart()
}

func (sc *Client) SetupSocket() {
	sc.BanningManager.CheckBan(sc.RemoteAddress.String())
	once := true

	go func() {
		logger.Log.Info().Str("from", sc.GetLabel()).Uint64("subscriptionID", sc.SubscriptionId).Msg("Connection established successfully, accepting data...")
		for {
			select {
			default:
				from := sc.GetLabel()
				raw, err := sc.SocketBufIO.ReadBytes('\n')
				if err != nil {
					if err == io.EOF {
						logger.Log.Info().Str("from", from).Uint64("subscriptionID", sc.SubscriptionId).Msg("Received EOF, closing connection")
						sc.SocketClosedEvent <- struct{}{}
						return
					}

					e, ok := err.(net.Error)
					if !ok {
						logger.Log.Error().Err(err).Str("from", from).Uint64("subscriptionID", sc.SubscriptionId).Msg("Failed to read bytes from socket due to non-network error")
						sc.SocketClosedEvent <- struct{}{}
						return
					}

					if ok && e.Timeout() {
						logger.Log.Error().Err(err).Str("from", from).Uint64("subscriptionID", sc.SubscriptionId).Msg("Socket is timeout")
						sc.SocketClosedEvent <- struct{}{}
						return
					}

					if ok && e.Temporary() {
						logger.Log.Error().Err(err).Str("from", from).Msg("Failed to read bytes from socket due to temporary error")
						continue
					}

					logger.Log.Error().Err(err).Str("from", from).Msg("Failed to read bytes from socket")
					sc.SocketClosedEvent <- struct{}{}
					return
				}

				// Check for message flood
				if len(raw) > maxMessageSize {
					logger.Log.Warn().
						Str("from", from).
						Str("raw_data", string(raw)).
						Int("data_size", len(raw)).
						Int("max_data_size", maxMessageSize).
						Msg("Flooding message")
					sc.SocketClosedEvent <- struct{}{}
					return
				}

				logger.Log.Trace().Str("from", from).Str("data", string(raw)).Int("data_size", len(raw)).Msg("Received new message")

				if len(raw) == 0 {
					continue
				}

				messageValid := true
				var message types.RPCRequest
				var loginMessage types.RPCLoginMessage
				err = json.Unmarshal(raw, &message)

				if err != nil {
					messageValid = false
					err = json.Unmarshal(raw, &loginMessage)
				}

				if err != nil {
					if !sc.Options.Stratum.TcpProxyProtocol {
						logger.Log.Error().Str("from", from).Str("label", sc.GetLabel()).Str("data", string(raw)).Msg("Malformed message")
						sc.SocketClosedEvent <- struct{}{}
					}

					return
				}

				if once && sc.Options.Stratum.TcpProxyProtocol {
					once = false
					if bytes.HasPrefix(raw, []byte("PROXY")) {
						sc.RemoteAddress, err = net.ResolveTCPAddr("tcp", string(bytes.Split(raw, []byte(" "))[2]))
						if err != nil {
							logger.Log.Error().Err(err).Msg("Failed to resolve tcp addr behind proxy")
						}
					} else {
						logger.Log.Error().Str("from", from).Str("raw data", string(raw)).
							Msg("Client IP detection failed, tcpProxyProtocol is enabled yet did not receive proxy protocol message, instead got data")
					}
				}
				// todo: handle banning
				sc.BanningManager.CheckBan(sc.RemoteAddress.String())

				if messageValid {
					sc.HandleMessage(&message)
				} else {
					sc.HandleLogin(&loginMessage, true)
				}
			}
		}
	}()
}

func (sc *Client) HandleMessage(message *types.RPCRequest) {
	switch message.Method {
	case types.Subscribe:
		sc.HandleSubscribe(message)
	case types.Authorize:
		sc.HandleAuthorize(message, true)
	case types.Submit:
		sc.LastActivity = time.Now()
		sc.HandleSubmit(message)
	case types.ExtranonceSubscribe:
		sc.SendJSON(types.AuthResponse(message.ID, true))
	case types.UpdateBlock:
		sc.HandleUpdateBlock(message)
	case types.Configure:
		sc.HandleConfigure(message)
	default:
		logger.Log.Warn().Interface("message", message).Msg("Unknown stratum method")
	}
}

func (sc *Client) HandleSubscribe(message *types.RPCRequest) {
	logger.Log.Info().Msg("Handling subscribe")

	sc.IsAuthorized = false

	// First item in message.Params is user agent.
	// Second is subscriptionID number in decimal format or null
	if len(message.Params) > 1 {
		logger.Log.Info().Str("workerName", sc.GetLabel()).Msg("Miner wants to resume context after reconnect")

		// Miner wants to resume context before reconnection
		idStr := string(message.Params[1])
		if idStr != "null" {
			idStr = strings.TrimPrefix(idStr, `"`)
			idStr = strings.TrimSuffix(idStr, `"`)

			id, err := strconv.ParseInt(idStr, 10, 64)
			if err != nil {
				logger.Log.Trace().Str("idStr", idStr).Msg("Unable to parse connection id")
			} else {
				val, found := clientCtxPool.Get(fmt.Sprintf("%d", id))
				if found {
					sc.IsAuthorized = true
					ctx := val.(clientConnCtx)
					sc.SubscriptionId = uint64(id)
					sc.ExtraNonce1 = ctx.extraNonce1
					sc.WorkerName = ctx.workerName

					// Update difficulty options
					if ctx.clientOptions != nil {
						sc.VarDiffUpdate(ctx.clientOptions)
					}

					// Send data when worker reconnected
					sc.Metrics.Activity.Reconnected = time.Now().Unix()
					logger.Log.Info().Str("workerName", sc.GetLabel()).Msg("Resumed context after reconnect")
				}
			}
		} else {
			logger.Log.Trace().Str("workerName", sc.GetLabel()).Msg("Connection id is null")
		}
	}

	if !sc.IsAuthorized {
		logger.Log.Info().Str("workerName", sc.GetLabel()).Msg("Unable to resume previous connection context, ask client to authenticate")
		// New connection
		connCtx := clientConnCtx{}
		clientCtxPool.Set(fmt.Sprintf("%d", sc.SubscriptionId), connCtx, cache.DefaultExpiration)
	}

	sc.SendJSON(types.SubscribeResponse(message.ID, sc.SubscriptionId, sc.ExtraNonce1))

	// Send current difficulty after subscription.
	if sc.IsAuthorized {
		sc.InitPoolDifficulty()
	}
}

func (sc *Client) HandleLogin(message *types.RPCLoginMessage, replyToSocket bool) {
	loginData := message.Params

	logger.Log.Info().Str("login", loginData.Login).Str("pass", loginData.Pass).
		Str("agent", loginData.Agent).Msg("Handling login")

	sc.WorkerName = loginData.Login
	sc.WorkerName = strings.TrimPrefix(sc.WorkerName, `"`)
	sc.WorkerName = strings.TrimSuffix(sc.WorkerName, `"`)

	sc.WorkerPass = loginData.Pass
	sc.WorkerPass = strings.TrimPrefix(sc.WorkerPass, `"`)
	sc.WorkerPass = strings.TrimSuffix(sc.WorkerPass, `"`)

	err := sc.Authorize()

	if replyToSocket {
		if sc.IsAuthorized {
			sc.SendJSON(types.AuthResponse(message.ID, sc.IsAuthorized))
		} else {
			sc.SendJSON(types.ErrorResponse(message.ID, 20, string(types.ToRawJSON(err))))
		}
	}

	if err != nil {
		logger.Log.Warn().Err(err).Str("worker", sc.WorkerName).Msg("Closed socket due to failed to authorize the miner")
		sc.SocketClosedEvent <- struct{}{}
		return
	}

	sc.InitPoolDifficulty()
}

// VarDiffUpdate changes variable difficulty options for client connection on the fly.
func (sc *Client) VarDiffUpdate(clientOptions map[string]float64) {
	if !sc.Options.Stratum.Ports.VarDiff.Enabled {
		if value, ok := clientOptions["d"]; ok {
			sc.CurrentDifficulty = big.NewFloat(value)
			sc.PendingDifficulty = big.NewFloat(0)
		}
		return
	}

	for key, value := range clientOptions {
		switch key {
		case "d":
			sc.varDiff.FixDiff = value
		case "dstart":
			sc.varDiff.StartDiff = value
		case "dmin":
			sc.varDiff.MinDiff = value
		case "dmax":
			sc.varDiff.MaxDiff = value
		case "starget":
			sc.varDiff.TargetSharesPerTimeWindow = uint32(value)
		case "timewindow":
			sc.varDiff.TimeWindow = uint64(value)
		case "retargettimewindow":
			sc.varDiff.RetargetTimeWindow = uint64(value)
		case "jobpriority":
			sc.varDiff.JobPriority = value != 0
		default:
		}
	}
}

func (sc *Client) Authorize() error {
	authorized, clientOptions, err :=
		AuthorizeFn(sc.RemoteAddress, sc.Socket.LocalAddr().(*net.TCPAddr).Port, sc.WorkerName, sc.WorkerPass)
	sc.IsAuthorized = err == nil && authorized

	if err != nil {
		return err
	}

	// Update difficulty options
	sc.VarDiffUpdate(clientOptions)
	logger.Log.Trace().Str("worker_name", sc.WorkerName).Bool("is_authorized", sc.IsAuthorized).Msg("VarDiff updated")

	// Cache client connection context
	connCtx := clientConnCtx{
		workerName:    sc.WorkerName,
		extraNonce1:   sc.ExtraNonce1,
		clientOptions: clientOptions,
	}
	clientCtxPool.Set(fmt.Sprintf("%d", sc.SubscriptionId), connCtx, cache.DefaultExpiration)

	// Send data when worker connected
	sc.Metrics.Activity.Connected = time.Now().Unix()

	logger.Log.Info().Str("worker_name", sc.WorkerName).Bool("is_authorized", sc.IsAuthorized).Msg("Worker authorized")
	return nil
}

func (sc *Client) HandleAuthorize(message *types.RPCRequest, replyToSocket bool) {
	logger.Log.Info().Msg("Handling authorize")

	sc.WorkerName = string(message.Params[0])
	sc.WorkerName = strings.TrimPrefix(sc.WorkerName, `"`)
	sc.WorkerName = strings.TrimSuffix(sc.WorkerName, `"`)

	sc.WorkerPass = string(message.Params[1])
	sc.WorkerPass = strings.TrimPrefix(sc.WorkerPass, `"`)
	sc.WorkerPass = strings.TrimSuffix(sc.WorkerPass, `"`)

	err := sc.Authorize()

	if replyToSocket {
		if sc.IsAuthorized {
			sc.SendJSON(types.AuthResponse(message.ID, sc.IsAuthorized))
		} else {
			sc.SendJSON(types.ErrorResponse(message.ID, 20, string(types.ToRawJSON(err))))
		}
	}

	if err != nil {
		logger.Log.Warn().Err(err).Str("worker", sc.WorkerName).Msg("Closed socket due to failed to authorize the miner")
		sc.SocketClosedEvent <- struct{}{}
		return
	}

	sc.InitPoolDifficulty()
}

// AuthorizeFn validates worker name and rig name.
func AuthorizeFn(ip net.Addr, port int, workerName string, password string) (authorized bool, options map[string]float64, err error) {
	logger.Log.Info().
		Str("workerName", workerName).
		Str("password", password).
		Str("ipAddr", ip.String()).
		Int("port", port).Msg("Authorize worker")

	var miner, rig string
	names := strings.Split(workerName, ".")
	if len(names) < 2 {
		miner = names[0]
		rig = "unknown"
	} else {
		miner = names[0]
		rig = names[1]
	}

	// JMP-64. Miner/rig validation.
	// Miner should be in UUID v4 format.
	_, err = uuid.Parse(miner)
	if err != nil {
		// TODO: use miner bitcoin address as worker name
		// return false, true, fmt.Errorf("%s", types.ErrMinerInvalidFormat)
	}

	// Rig is a string with [a-zA-Z0-9]+ regexp format.
	r, _ := regexp.Compile("[a-zA-Z0-9]+")
	if !r.MatchString(rig) {
		return false, nil, fmt.Errorf("%s", types.ErrRigInvalidFormat)
	}

	// Optional password. If it has format d={float64} then use this value as pool difficulty.
	if password != "" {
		options, err = utils.ExtractOptionsFromPassword(password)
		if err != nil {
			logger.Log.Info().Msg("invalid options in password, ignoring options")
			// return false, nil, fmt.Errorf("%s", types.ErrMinerPasswordInvalid)
			return true, map[string]float64{}, nil
		}

		return true, options, nil
	}

	return true, nil, nil
}

func (sc *Client) HandleSubmit(message *types.RPCRequest) {
	/* Avoid hash flood */
	if !sc.IsAuthorized {
		logger.Log.Error().Msg("Unauthorized worker submitted a share")
		sc.SendJSON(types.ErrorResponse(message.ID, 24, "unauthorized worker"))
		sc.ShouldBan()
		return
	}

	// todo: do this on generic way.
	data, _ := json.Marshal(message)
	var request types.SubmitRequest
	err := json.Unmarshal(data, &request)
	if err != nil {
		logger.Log.Error().Err(err).Msg("Unable to unmarshal request")
		sc.SendJSON(types.ErrorResponse(message.ID, 20, "unauthorized worker"))
		return
	}

	// Increase connection deadline on each subsequent message
	deadline := time.Now().Add(time.Duration(sc.Options.Stratum.ConnectionTimeout) * time.Second)
	err = sc.Socket.SetDeadline(deadline)
	if err != nil {
		logger.Log.Error().Err(err).Str("from", sc.GetLabel()).Msg("Failed to increase connection deadline")
		return
	} else {
		logger.Log.Trace().Str("from", sc.GetLabel()).Int64("deadline", deadline.Unix()).Msg("Increased connection deadline")
	}

	now := time.Now()

	// TODO: TMP debug for nil sc.ClientTarget
	if sc.ClientTarget == nil {
		logger.Log.Error().Msg("Client target is nil")

		sc.ClientTarget = utils.Difficulty2Target(new(big.Float).SetInt64(256000))

		_, err := fmt.Fprintln(os.Stderr, spew.Sdump(sc))
		if err != nil {
			logger.Log.Error().Err(err).Msg("Failed to write fatal error to Stderr")
		}
	}
	//

	share, minerResult := sc.JobManager.
		ProcessSubmit(
			request.Params.JobID,
			sc.ExtraNonce1,                // extraNonce1 uint32
			request.Params.HexExtraNonce2, // hexExtraNonce2
			request.Params.HexNTime,       // hexNTime
			request.Params.HexNonce,       // hexNonce
			request.Params.VersionBits,    // versionBits
			sc.versionRolling,             // version rolling support
			sc.RemoteAddress,              // ipAddr net.Addr
			request.Params.WorkerName,     // workerName
			sc.ClientTarget,               // client target from connection options
		)
	logger.Log.Trace().Int64("duration", time.Since(now).Nanoseconds()).Msg("Profiling ProcessSubmit after optimisation")

	if share != nil {
		sc.varDiff.AppendShare(time.Now().Unix())

		if share.ErrorCode == types.ErrLowDiffShare {
			// warn the miner with current diff
			if sc.CurrentDifficulty != nil {
				diff, _ := sc.CurrentDifficulty.Float64()
				logger.Log.Warn().Float64("currDiff", diff).Msg("Submitted share with low difficulty, discarding share")
			} else {
				logger.Log.Error().Msg("Current difficulty is nil")
			}

			return
		}

		if share.ErrorCode == types.ErrNTimeOutOfRange {
			logger.Log.Warn().Msg("Submitted share with invalid NTime parameter, discarding share")
			return
		}

		if sc.ShouldBan() {
			return
		}

		var errParams *types.JsonRpcError
		if share.ErrorCode != 0 {
			errParams = &types.JsonRpcError{
				Code:    int(share.ErrorCode),
				Message: share.ErrorCode.String(),
			}

			logger.Log.Error().Str("errorMessage", errParams.Message).Str("workerName", sc.WorkerName).
				Msg("Share is invalid")
			sc.SendJSON(types.ErrorResponse(message.ID, int(share.ErrorCode), share.ErrorCode.String()))

			// Update worker stats
			sc.Metrics.SharesPerMinute.IncrementInvalid()
			sc.Metrics.Shares.IncrementInvalid()
			return
		}

		sc.SendJSON(types.AuthResponse(message.ID, true))

		// Update worker stats
		sc.Metrics.SharesPerMinute.IncrementValid()
		sc.Metrics.Shares.IncrementValid()

		// send result to channel if at least one block found
		if minerResult.ContainsMinedBitcoinBlock() || minerResult.ContainsMinedBeaconBlock() || minerResult.ContainsMinedShardsBlocks() {
			logger.Log.Info().Bool("blockMined", true).Str("workerName", sc.WorkerName).Str("jobId", share.JobId).Msg("Submitted a valid share")
			sc.JobManager.MinerResultChan <- minerResult
		} else {
			logger.Log.Info().Bool("blockMined", false).Str("workerName", sc.WorkerName).Str("jobId", share.JobId).Msg("Submitted a valid share")
		}
	}
}

func (sc *Client) SendJSON(data interface{}) {
	raw, err := json.Marshal(data)
	if err != nil {
		logger.Log.Error().Err(err).Interface("data", data).Msg("failed to serialize JSON")
		return
	}

	message := make([]byte, 0, len(raw)+1)
	message = append(raw, '\n')
	sc.SocketBufIOMutex.Lock()
	_, err = sc.SocketBufIO.Write(message)
	if err != nil {
		sc.SocketBufIOMutex.Unlock()
		logger.Log.Error().Err(err).Str("rawData", string(raw)).Msg("Failed to write data into socket")
		sc.SocketClosedEvent <- struct{}{}
		return
	}

	err = sc.SocketBufIO.Flush()
	if err != nil {
		sc.SocketBufIOMutex.Unlock()
		logger.Log.Error().Err(err).Str("workerName", sc.WorkerName).Msg("Failed to send data for worker")
		sc.SocketClosedEvent <- struct{}{}
		return
	}

	sc.SocketBufIOMutex.Unlock()
	logger.Log.Trace().Str("rawData", string(raw)).Str("workerName", sc.WorkerName).Msg("Sent raw bytes")
}

func (sc *Client) ShouldBan() bool {
	if !sc.BanningManager.Enabled {
		return false
	}

	if sc.Metrics.SharesPerMinute.TotalShares() >= sc.Options.Stratum.Banning.CheckThreshold {
		if sc.Metrics.SharesPerMinute.BadPercent() < sc.Options.Stratum.Banning.InvalidPercent {
			return false
			// sc.Metrics.SharesPerMinute.Reset()
		} else {
			logger.Log.Info().Str("invalidShares", strconv.FormatUint(sc.Metrics.SharesPerMinute.Invalid, 10)).Str("totalShares", strconv.FormatUint(sc.Metrics.Shares.TotalShares(), 10)).Send()
			sc.BanningManager.AddBannedIP(sc.RemoteAddress.String())
			logger.Log.Warn().Str("", sc.WorkerName).Msg("Closed socket due to shares bad percent")

			// TODO: create function sc.CloseConnection() as high level abstraction
			sc.SocketClosedEvent <- struct{}{}
			return true
		}
	}
	return false
}

func (sc *Client) GetLabel() string {
	if sc.WorkerName != "" {
		return sc.WorkerName + " [" + sc.RemoteAddress.String() + "]"
	} else {
		return "(unauthorized)" + " [" + sc.RemoteAddress.String() + "]"
	}
}

func BitsToStr(bits uint32) string {
	target := pow.CompactToBig(bits)
	return fmt.Sprintf("%064x", target)
}

func (sc *Client) SendDifficulty(diff *big.Float) bool {
	if diff == nil {
		logger.Log.Error().Msg("Trying to send nil diff")
		return false
	}

	sc.CurrentDifficulty = diff
	f, _ := diff.Float64()
	sc.SendJSON(types.SetDifficultyResponse(0, f)) // TODO: Send correct id

	return true
}

// InitPoolDifficulty calculates initial pool difficulty when new
// stratum client is connected and sends it to the client.
func (sc *Client) InitPoolDifficulty() {
	minDiff := big.NewFloat(minPoolDifficulty)
	var poolDiff *big.Float

	if sc.Options.Stratum.Ports.VarDiff.Enabled {
		// Variable difficulty
		if sc.varDiff.JobPriority {
			poolDiff = minDiff
			sc.varDiff.CurrDiff = minPoolDifficulty
		} else {
			if sc.varDiff.FixDiff > 0.0 {
				poolDiff = big.NewFloat(sc.varDiff.FixDiff)
			} else {
				poolDiff = big.NewFloat(sc.varDiff.StartDiff)
				sc.varDiff.CurrDiff = sc.varDiff.StartDiff
			}
		}
	} else {
		// Fixed difficulty from password options or fixed minimum difficulty
		if sc.CurrentDifficulty != nil {
			poolDiff = sc.CurrentDifficulty
		} else {
			poolDiff = minDiff
		}
	}

	sc.ClientTarget = &sc.JobManager.CurrentJob.MinerTask.ShareTarget
	sc.SendDifficulty(poolDiff)
	logger.Log.Trace().
		Str("poolDiff", poolDiff.String()).
		Str("sc.ClientTarget", sc.ClientTarget.String()).
		Msg("InitPoolDifficulty")
}

func (sc *Client) SendMiningJob(jobParams types.JobParams) {
	if !sc.IsAuthorized {
		logger.Log.Trace().Str("from", sc.GetLabel()).Msg("Client is not authorised, skip sending job")
		return
	}

	lastActivityAgo := time.Since(sc.LastActivity)
	if lastActivityAgo > time.Duration(sc.Options.Stratum.ConnectionTimeout)*time.Second {
		sc.SocketClosedEvent <- struct{}{}
		logger.Log.Info().Str("sc.WorkerName", sc.WorkerName).Msg("Closed socket due to activity timeout")
		metrics.IncCounterVec(metrics.WorkerConnTimeout, map[string]string{"worker": sc.WorkerName})
		return
	}

	sc.SendJSON(types.NotifyMessageReq(0, jobParams))
}

func (sc *Client) ManuallyAuthClient(username, password string) {
	sc.HandleAuthorize(&types.RPCRequest{
		ID:     1,
		Method: "",
		Params: []json.RawMessage{types.ToRawJSON(username), types.ToRawJSON(password)},
	}, false)
}

func (sc *Client) ManuallySetValues(otherClient *Client) {
	sc.ExtraNonce1 = otherClient.ExtraNonce1
	sc.PreviousTarget = otherClient.PreviousTarget
	sc.CurrentTarget = otherClient.CurrentTarget
}

func (sc *Client) HandleUpdateBlock(message *types.RPCRequest) {
	if len(message.Params) < 2 {
		logger.Log.Trace().Str("reason", "not enough params").Msg("Update block method failed")
		sc.SendJSON(types.ErrorResponse(message.ID, 20, string(types.ToRawJSON("not enough params"))))
		return
	}

	token := string(message.Params[0])
	token = strings.TrimPrefix(token, `"`)
	token = strings.TrimSuffix(token, `"`)
	if token != sc.Options.Stratum.UpdateBlockToken {
		logger.Log.Trace().Str("want", sc.Options.Stratum.UpdateBlockToken).Str("got", token).Msg("Invalid update block token received")
		sc.SendJSON(types.ErrorResponse(message.ID, 24, string(types.ToRawJSON("invalid update block token"))))
		return
	}

	chain := string(message.Params[1])
	chainID, err := strconv.ParseInt(chain, 10, 64)
	if err != nil {
		logger.Log.Trace().Msg("Invalid chainID received")
		sc.SendJSON(types.ErrorResponse(message.ID, 24, string(types.ToRawJSON("invalid chain ID"))))
		return
	}

	sc.sendStratumUpdateBlockEvent <- events.StratumUpdateBlock{ChainID: chainID}
	sc.SendJSON(types.AuthResponse(message.ID, true))
}

// HandleConfigure handles mining.configure stratum method.
// Currently, only version-rolling extension is supported.
// min-bit-count field is considered obsolete and does not use in the code.
// Example of this request:
// {
//		"method": "mining.configure", "id": 1,
//		"params": [
//			["minimum-difficulty", "version-rolling"],
//			{
//				"minimum-difficulty.value":      2048,
//				"version-rolling.mask":          "1fffe000",
//				"version-rolling.min-bit-count": 2
//			}
//		]
// }
func (sc *Client) HandleConfigure(message *types.RPCRequest) {
	if len(message.Params) == 2 {
		extensions := string(message.Params[0])
		if strings.Contains(extensions, "version-rolling") {
			req := &types.VersionRollingReq{}
			err := json.Unmarshal(message.Params[1], req)
			if err != nil {
				logger.Log.Error().Err(err).Msg("Unable to unmarshal version rolling request")
				sc.SendJSON(types.ErrorResponse(message.ID, 24, string(types.ToRawJSON("unable to decode rolling request"))))
				return
			}

			mask, err := strconv.ParseUint(req.Mask, 16, 32)
			if err != nil {
				sc.SendJSON(types.ErrorResponse(message.ID, 24, string(types.ToRawJSON("invalid version rolling mask value"))))
				return
			}

			var clientMask uint32
			clientMask = versionRollingServerMask & uint32(mask)

			sc.versionRolling = types.VersionRolling{
				Supported: true,
				Mask:      clientMask,
			}

			sc.SendJSON(types.VersionRollingResponse(message.ID, clientMask))
		}
	} else {
		sc.SendJSON(types.ErrorResponse(message.ID, 24, string(types.ToRawJSON("invalid configure request"))))
		return
	}
}

// VarDiffStart starts variable difficulty feature.
// Every RetargetTimeWindow represented in seconds it calculates next client difficulty.
// It stops when receives a data via stopChan channel.
func (sc *Client) VarDiffStart() {
	if !sc.varDiff.Enabled {
		logger.Log.Trace().Msg("VarDiff disabled, return")
		return
	}

	go func() {
		dur := time.Second * time.Duration(sc.varDiff.RetargetTimeWindow)
		retargetTimeWindowTicker := time.NewTicker(dur)
		defer retargetTimeWindowTicker.Stop()

		for {
			select {
			case <-sc.varDiff.StopChan:
				return

			case <-retargetTimeWindowTicker.C:
				sc.Metrics.SetHashRate(sc.varDiff.CalcNextDiff())
				sc.PublishPeriodicMetrics()

				if sc.varDiff.NewDiff != sc.varDiff.CurrDiff {
					logger.Log.Trace().Float64("sc.varDiff.CurrDiff", sc.varDiff.CurrDiff).Float64("sc.varDiff.NewDiff", sc.varDiff.NewDiff).Msg("VarDiff updated")
					bigDiff := big.NewFloat(sc.varDiff.NewDiff)
					sc.varDiff.CurrDiff = sc.varDiff.NewDiff
					sc.SendDifficulty(bigDiff)
				}
			}
		}
	}()
}

func (sc *Client) VarDiffStop() {
	sc.varDiff.StopChan <- struct{}{}
	close(sc.varDiff.StopChan)
}

func (sc *Client) Disconnect() {
	// Close tcp connection
	logger.Log.Trace().Uint64("subscriptionID", sc.SubscriptionId).Msg("Received client SocketClosedEvent in server")
	err := sc.Socket.Close()
	if err != nil {
		logger.Log.Error().Err(err).Str("from", sc.Socket.RemoteAddr().String()).Uint64("subscriptionID", sc.SubscriptionId).Msg("Failed to close miner connection")
	}
	logger.Log.Info().Str("from", sc.Socket.RemoteAddr().String()).Uint64("subscriptionID", sc.SubscriptionId).Msg("Closed miner connection")

	// Stop calculating variable difficulty
	sc.VarDiffStop()

	// Clean up some metrics
	sc.CleanupMetrics()
}

// CleanupMetrics cleans up some metrics on disconnect.
// Otherwise they will have most recent value and
// show incorrect values when observing in Grafana for example.
func (sc *Client) CleanupMetrics() {
	workerID := strconv.Itoa(int(sc.ExtraNonce1))

	metrics.DeleteGaugeVec(metrics.SubmittedShares,
		map[string]string{
			"worker":       sc.WorkerName,
			"valid_shares": "true",
			"id":           workerID,
		},
	)

	metrics.DeleteGaugeVec(metrics.SubmittedShares,
		map[string]string{
			"worker":       sc.WorkerName,
			"valid_shares": "false",
			"id":           workerID,
		},
	)

	metrics.DeleteGaugeVec(metrics.HashRate,
		map[string]string{
			"worker": sc.WorkerName,
			"id":     workerID,
		},
	)
}

// CalcHashRate returns human form of hash rate value
func (sc *Client) CalcHashRate() string {
	const teraPower = 12
	const gigaPower = 9
	const megaPower = 6
	const kiloPower = 3
	var format = "%.2f"

	hashRate := float64(sc.varDiff.CalcHashRate())

	var teraHash = math.Pow10(teraPower)
	var gigaHash = math.Pow10(gigaPower)
	var megaHash = math.Pow10(megaPower)
	var kiloHash = math.Pow10(kiloPower)

	if hashRate > teraHash {
		hashRate = hashRate / teraHash
		format = "%.2fT"
	} else if hashRate > gigaHash {
		hashRate = hashRate / gigaHash
		format = "%.2fG"
	} else if hashRate > megaHash {
		hashRate = hashRate / megaHash
		format = "%.2fM"
	} else if hashRate > kiloHash {
		hashRate = hashRate / kiloHash
		format = "%.2fK"
	}

	return fmt.Sprintf(format, hashRate)
}

// newMetrics returns initialised Metrics instance for Client type
func newMetrics() *Metrics {
	m := new(Metrics)
	m.Shares = new(Shares)
	m.SharesPerMinute = new(Shares)
	m.Activity = new(Activity)

	return m
}

// PublishPeriodicMetrics produces periodic metrics.
func (sc *Client) PublishPeriodicMetrics() {
	workerID := strconv.Itoa(int(sc.ExtraNonce1))

	metrics.SetGaugeVec(metrics.SubmittedShares,
		map[string]string{
			"worker":       sc.WorkerName,
			"valid_shares": "true",
			"id":           workerID,
		},
		float64(sc.Metrics.SharesPerMinute.GetValid()),
	)

	metrics.SetGaugeVec(metrics.SubmittedShares,
		map[string]string{
			"worker":       sc.WorkerName,
			"valid_shares": "false",
			"id":           workerID,
		},
		float64(sc.Metrics.SharesPerMinute.GetInvalid()),
	)

	metrics.SetGaugeVec(metrics.HashRate,
		map[string]string{
			"worker": sc.WorkerName,
			"id":     workerID,
		},
		float64(sc.Metrics.GetHashRate()),
	)

	logger.Log.Info().Str("worker", sc.WorkerName).
		Uint64("last_minute_valid", sc.Metrics.SharesPerMinute.GetValid()).
		Uint64("last_minute_invalid", sc.Metrics.SharesPerMinute.GetInvalid()).
		Uint64("total_valid", sc.Metrics.Shares.GetValid()).
		Uint64("total_invalid", sc.Metrics.Shares.GetInvalid()).
		Str("sc.CalcHashRate()", sc.CalcHashRate()).
		Uint64("sc.Metrics.GetHashRate()", sc.Metrics.GetHashRate()).
		Msg("Submitted shares by worker")

	sc.Metrics.SharesPerMinute.Reset()
	sc.Metrics.ResetHashRate()
}
